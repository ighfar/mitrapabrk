require('dotenv').config();
const jwt = require("jsonwebtoken");

function authProtocol(token, duration, KEY) {
    let result = {status:401,user:{}};
    if(!token) {
        result.msg = "token not found";
    } else {
        try {
            result.user = jwt.verify(token, KEY);
            if(!result.user.rememberMe) {     
                if((new Date().getTime() / 1000) - result.user.iat > duration) {
                    result.status = 403
                    result.msg = "Token Expired.";
                }
                else {
                    result.status = 200;
                }
            } else {
                result.status = 200;
            }
        } catch (error) {
            result.msg = "Invalid Token!";
            result.error = error;
        }
    }
    return result;
}

function verifyEmailToken(token) {
    try {
        let user = jwt.verify(token, process.env.PRIVATE_TOKEN_KEY_VERIFY);
        if((new Date().getTime() / 1000) - user.iat > 60 * 5) {
            return { status:-1, data:user }
        }
        else {
            return { status:1, data:user }
        }
    } catch (error) {
        return { status:-1, data:error }
    }
}

function authUserToken(token) {
    return authProtocol(token, 3600 * 24 * 7, process.env.PRIVATE_TOKEN_KEY);
}

function authAdminToken(token) {
    return authProtocol(token, 3600 * 24 * 7, process.env.PRIVATE_TOKEN_KEY_ADMIN);
}

function authEmailToken(token) {
    return authProtocol(token, 3600 * 24 * 7, process.env.PRIVATE_TOKEN_KEY_VERIFY);
}

function createUserToken(data) {
    return jwt.sign(data, process.env.PRIVATE_TOKEN_KEY);
}

function createAdminToken(data) {
    return jwt.sign(data, process.env.PRIVATE_TOKEN_KEY_ADMIN);
}

function createEmailToken(data) {
    return jwt.sign(data, process.env.PRIVATE_TOKEN_KEY_VERIFY);
}

module.exports = {
    authProtocol,
    authUserToken,
    authEmailToken,
    createUserToken,
    createAdminToken,
    authAdminToken,
    createEmailToken,
    verifyEmailToken
};