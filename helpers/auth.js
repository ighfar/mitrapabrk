const jwt = require("jsonwebtoken");

const isValidToken = (key, token) => {
  const KEY = key || process.env.PRIVATE_TOKEN_KEY;
  return token && jwt.verify(token, KEY);
};

module.exports = {
  isValidToken,
};
