const onlyMessage = ({ location, msg, param, value, nestedErrors }) => {
  return msg;
};

module.exports = {
  onlyMessage,
};
