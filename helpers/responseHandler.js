const responseHandler = async (controllerFunc, res) => {
  const isFunction = typeof controllerFunc === "function";
  const isPromise = controllerFunc instanceof Promise;
  const isObject = typeof controllerFunc === "object";

  if (isPromise) {
    const isFunctionPromise = typeof controllerFunc.then === "function";
    console.log("isFunctionPromise", isFunctionPromise);

    if (isFunctionPromise) {
      return await controllerFunc
        .then((data) => {
          res.status(200).json(responseSuccess(data));
        })
        .catch((error) => {
          res.status(500).json(responseError(error));
        });
    }
  } else if (isFunction) {
    const result = controllerFunc();
    return res.status(200).json(responseSuccess(result));
  } else if (isObject) {
    try {
      const result = await controllerFunc;
      return res.status(200).json(responseSuccess(result));
    } catch (error) {
      return res.status(500).json(responseError(error));
    }
  } else {
    return res.status(500).json(responseError("Invalid controller function"));
  }
};

const responseSuccess = (result) => {
  console.log("result", result);
  if (typeof result !== "array" && typeof result !== "object") {
    result = [result];
  }
  return {
    errors: null,
    data: result,
  };
};

const responseError = (error) => {
  if (typeof error !== "array" && typeof error !== "object") {
    error = [error];
  } else if (typeof error === "object" && error.message) {
    error = [error.message];
  }
  return {
    errors: error,
    data: null,
  };
};

module.exports = responseHandler;
