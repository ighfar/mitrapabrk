const QRCode = require("qrcode");

const generateQR = async (id_event) => {
  try {
    return await QRCode.toDataURL(urlEvent(id_event));
  } catch (err) {
    throw new Error(err);
  }
};

const urlEvent = (id_event) => {
  const domain = process.env.DOMAIN || "";
  return `${domain}/events/attendance/${id_event}`;
};

module.exports = {
  generateQR,
};
