const fs = require("fs");
const { v1: uuidv1 } = require("uuid");
const s3 = require("../helpers/aws");

const validateImage = (file) => {
  if (!file) throw new Error("No file uploaded");
  if (!file.mimetype.startsWith("image"))
    throw new Error("Please upload an image file");
};

const upload = async ({ type, file, path, bucket }) => {
  if (type == "image") return await handleUploadImage({ file, path, bucket });
};

const handleUploadImage = async ({ file, path, bucket }) => {
  validateImage(file);
  const fileExtension = file.name.split(".").pop();
  const fileName = uuidv1().toString() + "." + fileExtension;
  try {
    file.mv(`${path}/${fileName}`);

    // read photo file and upload to digital ocean spaces using aws sdk
    const fileStream = fs.createReadStream(`${path}/${fileName}`);
    fileStream.on("error", function (err) {
      throw new Error(err);
    });
    const uploadParams = {
      Bucket: bucket,
      Key: fileName,
      Body: fileStream,
      ACL: "public-read",
      ContentType: file.mimetype,
    };
    const result = await s3.upload(uploadParams).promise();

    fs.unlinkSync(`${path}/${fileName}`);
    return {
      fileName,
      url: result.Location,
    };
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = { upload };
