const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");
const plugins = require("../plugins");

let readHTMLFile = function (path, callback) {
  fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
    if (err) callback(err);
    else callback(null, html);
  });
};

const sendVerificationEmail = (hash, { email, nama_lengkap }) => {
  const domain = process.env.DOMAIN_FRONTEND || "http://localhost:3000";
  return readHTMLFile(
    path.join(__dirname, "../", "asset", "verify-body.html"),
    async function (err, html) {
      const template = handlebars.compile(html);
      const replacements = {
        verifyLink: `${domain}/?t=${hash}`,
        nama_lengkap: nama_lengkap
      };
      const htmlToSend = template(replacements);
      return await plugins.handleMailerAPI(
        "https://api.elasticemail.com/v4/emails/transactional",
        `Dear. ${plugins.decodeHtml(
          nama_lengkap
        )}, Silahkan Verifikasi Email Anda`,
        htmlToSend,
        email
      );
    }
  );
};

const sendEmailResetPassword = (hash, { email, nama_lengkap, password_nohash }) => {
  const domain = process.env.DOMAIN_FRONTEND || "http://localhost:3000";
  return readHTMLFile(
    path.join(__dirname, "../", "asset", "password-reset.html"),
    async function (err, html) {
      const template = handlebars.compile(html);
      const replacements = {
        nama_lengkap: nama_lengkap,
        password_nohash: password_nohash
      };
      const htmlToSend = template(replacements);
      return await plugins.handleMailerAPI(
        "https://api.elasticemail.com/v4/emails/transactional",
        `Dear. ${plugins.decodeHtml(
          nama_lengkap
        )}, berikut adalah password anda sementara`,
        htmlToSend,
        email
      );
    }
  );
};


module.exports = {
  sendVerificationEmail,
  sendEmailResetPassword,
};
