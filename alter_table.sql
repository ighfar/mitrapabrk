ALTER TABLE `user`
ADD `gender` varchar(40) COLLATE 'utf8mb4_general_ci' NULL AFTER `nama_belakang`,
ADD `tanggal_lahir` date NULL AFTER `gender`,
ADD `nama_direktur` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `nama_perusahaan`,
ADD `nomer_telepone` varchar(100) COLLATE 'utf8mb4_general_ci' NULL AFTER `alamat_perusahaan`,
ADD `nomer_fax` varchar(100) COLLATE 'utf8mb4_general_ci' NULL AFTER `alamat_perusahaan`,
ADD `email_perusahaan` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `nomer_fax`,
ADD `email_secondary` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `email_perusahaan
ADD `nomer_tdp` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `email_perusahaan`,
ADD `information_about` varchar(100) COLLATE 'utf8mb4_general_ci' NULL AFTER `pocketbase_id`,
ADD `nama_lengkap` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `password`,
ADD `alamat` text COLLATE 'utf8mb4_general_ci' NULL AFTER `asal_kota,
ADD `goal_join` varchar(100) COLLATE 'utf8mb4_general_ci' NULL AFTER `information_about`;

ALTER TABLE `user`
DROP `password_nohash`;

ALTER TABLE `user`
CHANGE `timestamp` `timestamp` timestamp NULL ON UPDATE CURRENT_TIMESTAMP AFTER `notice_create`;

ALTER TABLE `products`
ADD `sale` tinyint(2) NULL COMMENT '0' AFTER `faq`,
ADD `material` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `faq`,
ADD `kategori_id` int(11) NULL AFTER `id_partner`,
ADD `diskon` int(4) NULL AFTER `sale`;

DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `order` int(6) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `products`
CHANGE `title` `title` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `id_partner`,
CHANGE `kategori_id` `kategori_id` int(11) NULL AFTER `title`;

ALTER TABLE `showroom`
ADD `detail_alamat` text COLLATE 'utf8mb4_general_ci' NULL AFTER `alamat`,
ADD `galery` text COLLATE 'utf8mb4_general_ci' NULL,
ADD `deskripsi` text COLLATE 'utf8mb4_general_ci' NULL AFTER `detail_alamat`;

DROP TABLE IF EXISTS `product_images`;
CREATE TABLE `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `path` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO product_images (product_id, path)
SELECT
  products.id_produk as product_id,
  SUBSTRING_INDEX(SUBSTRING_INDEX(products.katalog, ',', numbers.n), ',', -1) path
FROM
  (SELECT 1 n UNION ALL SELECT 2
   UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5
UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8
UNION ALL SELECT 9 UNION ALL SELECT 10 UNION ALL SELECT 11
UNION ALL SELECT 12 UNION ALL SELECT 13 UNION ALL SELECT 14
UNION ALL SELECT 15 UNION ALL SELECT 16) numbers INNER JOIN products
  ON CHAR_LENGTH(products.katalog)
     -CHAR_LENGTH(REPLACE(products.katalog, ',', ''))>=numbers.n-1
ORDER BY
  id_produk, n