const { expect } = require("chai");
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const DB = require("../models/index");
require("dotenv").config();

const should = chai.should();
chai.use(chaiHttp);

let projectId;
let materialId;
const emailUser = "john@test.com";
const defaultPassword = "asd1234567";
const headers = {
  "auth-token": "",
};

const project = {
  project_deadline: "2022-09-07 00:00:00",
  person_name: "Nama org from test",
  person_phone: "0888388888888",
  project_name: "refactor 3",
  project_address: "Alamat",
  project_city: "project kota",
  project_budget: "121212",
  project_note: "catatan project",
  project_status: "status project",
  project_photo: "94rjeondf4hfeanfcoa8r439g4fka4wrgwnedfi.jpg",
};

const projectInvalid = {
  project_deadline: "2022-09-07 00:00:00",
  person_name: "Nama org from test",
  person_phone: "0888388888888",
  project_name: "",
  project_address: "Alamat",
  project_city: "project kota",
  project_budget: "121212",
  project_note: "catatan project",
  project_status: "status project",
  project_photo: "94rjeondf4hfeanfcoa8r439g4fka4wrgwnedfi.jpg",
};

const material = {
  type: "tipe material from test",
  brand: "brand material",
  material: "name material",
  area_needed: "12212",
  size: "2323",
  quantity: "3434",
  budget: "45334",
  photo: "94rjeondf4hfeanfcoa8r439g4fka4wrgwnedfi.jpg",
  project_id: "",
};

const materialInvalid = {
  type: "",
  brand: "brand material",
  material: "name material",
  area_needed: "12212",
  size: "2323",
  quantity: "3434",
  budget: "45334",
  photo: "94rjeondf4hfeanfcoa8r439g4fka4wrgwnedfi.jpg",
  project_id: "",
};

const headersTokenInvalid = {
  "auth-token":
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJUZXN0ZXIiLCJpYXQiOjE2NjI2MDUzODgsImV4cCI6MTY5NDE0MTM4OCwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoidGVzdEBleGFtcGxlLmNvbSIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJkb2UiLCJFbWFpbCI6InRlc3RAZXhhbXBsZS5jb20iLCJSb2xlIjoiTWFuYWdlciJ9.yzV6glKV0zwsqGsSVX3CLyfZG5echPurXSWmIHvTuv4",
};

describe("Tests material", () => {
  before(async () => {
    await DB.knex("user").where("email", emailUser).del();
  });

  describe("POST /api/user/register/quick", () => {
    it("it should register", (done) => {
      chai
        .request(server)
        .post("/api/user/register/quick")
        .send({
          nama_depan: "John",
          nama_belakang: "Doe",
          email: emailUser,
          nama_perusahaan: "Test Perusahaan",
          alamat_perusahaan: "Jl. Test",
          no_hp: "08123456789",
          minat: "Test Minat",
          role: "User",
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });

    describe("POST /api/user/login", () => {
      it("it should 401 unauthorized", (done) => {
        chai
          .request(server)
          .post("/api/user/login")
          .send({
            email: "wrong@user.com",
            password: "wrongpassword",
            role: "User",
          })
          .end((err, res) => {
            res.should.have.status(401);
            done();
          });
      });

      it("it should 200 success", (done) => {
        chai
          .request(server)
          .post("/api/user/login")
          .send({
            email: emailUser,
            password: defaultPassword,
          })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property("data");
            res.body.data.should.be.a("object");
            res.body.should.have.property("token");
            token = res.body.token;
            headers["auth-token"] = token;
            done();
          });
      });
    });

    describe("POST /api/project", () => {
      it("should return 422 fail (by validation)", (done) => {
        expect(headers["auth-token"]).to.not.be.empty;
        chai
          .request(server)
          .post("/api/project")
          .send(projectInvalid)
          .set(headers)
          .end((err, res) => {
            res.should.have.status(422);
            done();
          });
      });

      it("should return error (by middleware)", (done) => {
        chai
          .request(server)
          .post("/api/project")
          .send(project)
          .set(headersTokenInvalid)
          .end((err, res) => {
            res.should.have.status(500);
            done();
          });
      });

      it("should return 200 sucess", (done) => {
        expect(headers["auth-token"]).to.not.be.empty;
        expect(project).to.not.be.empty;
        expect(project).to.be.an("object");
        expect(project).to.have.property("project_deadline");
        expect(project).to.have.property("person_name");
        expect(project.project_name).to.be.a("string");
        expect(project.project_name).to.not.be.empty;
        expect(project).to.have.property("person_phone");
        expect(project).to.have.property("project_name");
        expect(project).to.have.property("project_address");
        expect(project).to.have.property("project_city");
        expect(project).to.have.property("project_budget");
        expect(project).to.have.property("project_note");
        expect(project).to.have.property("project_status");
        expect(project).to.have.property("project_photo");

        chai
          .request(server)
          .post("/api/project")
          .send(project)
          .set(headers)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.data.should.be.a("object");
            res.body.data.should.have.property("id");
            res.body.data.id.should.be.a("number");
            projectId = res.body.data.id;
            materialInvalid.project_id = projectId;
            material.project_id = projectId;
            describe("POST /api/material", () => {
              expect(projectId).to.be.a("number");

              it("should return 422 fail (by validation)", (done) => {
                chai
                  .request(server)
                  .post("/api/material")
                  .send(materialInvalid)
                  .set(headers)
                  .end((err, res) => {
                    res.should.have.status(422);
                    done();
                  });
              });

              it("should return error (by middleware)", (done) => {
                chai
                  .request(server)
                  .post("/api/material")
                  .send(material)
                  .set(headersTokenInvalid)
                  .end((err, res) => {
                    res.should.have.status(500);
                    done();
                  });
              });

              it("should return 200", (done) => {
                chai
                  .request(server)
                  .post("/api/material")
                  .send(material)
                  .set(headers)
                  .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property("data");
                    res.body.data.should.have.property("id");
                    res.body.data.id.should.be.a("number");
                    materialId = res.body.data.id;
                    done();
                  });
              });
            });
            done();
          });
      });
    });
  });

  after(() => {
    DB.knex("user").where("email", emailUser).del();
    DB.knex("project").where("id", projectId).del();
    DB.knex("material").where("id", materialId).del();
  });
});
