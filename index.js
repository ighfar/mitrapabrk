require("dotenv").config();
const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");

//routes
const userRoute = require("./routes/user");
const eventsRoute = require("./routes/events");
const artikelsRoute = require("./routes/artikels");
const productsRoute = require("./routes/products");
const requestsRoute = require("./routes/requests");
const showroomRoute = require("./routes/showroom");
const wishlistRoute = require("./routes/wishlist");
const materialRoute = require("./routes/material");
const projectRoute = require("./routes/project");
const noticeRoute = require("./routes/notices");
const replyRoute = require("./routes/reply");
const bannerRoute = require("./routes/banner");

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/uploads"));
app.use(express.json());
app.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: __dirname + "/uploads/tmp/",
  })
);

app.use("/api/user", userRoute);
app.use("/api/events", eventsRoute);
app.use("/api/artikels", artikelsRoute);
app.use("/api/products", productsRoute);
app.use("/api/requests", requestsRoute);
app.use("/api/showrooms", showroomRoute);
app.use("/api/wishlist", wishlistRoute);
app.use("/api/material", materialRoute);
app.use("/api/project", projectRoute);
app.use("/api/notices", noticeRoute);
app.use("/api/reply", replyRoute);
app.use("/api/banner", bannerRoute);

// app.use("api/health/sync", (_, res) => {
//   res.status(200).send("OK");
// });

// app.use("api/health/async", async (_, res) => {
//   res.status(200).send("OK");
// });

if (!module.parent) {
  app.listen(process.env.PORT, function () {
    console.log("Listening to port " + process.env.PORT);
  });
}

module.exports = app;
