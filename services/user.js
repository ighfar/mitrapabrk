const { isValidToken } = require("../helpers/auth");
const { verifyUserEmail } = require("../models/user");

const verifyEmail = async (token) => {
  const key = process.env.PRIVATE_TOKEN_KEY_VERIFY;
  const email = isValidToken(key, token);
  await verifyUserEmail(email);
  return "Email verified successfully!";
};

module.exports = {
  verifyEmail,
};
