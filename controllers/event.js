const QRCode = require("../helpers/QRCode");
const eventModel = require("../models/events");
const responseHandler = require("../helpers/responseHandler");

const generateQR = async (req, res) => {
  const id_event = req.query.id_event ? parseInt(req.query.id_event) : 0;

  if (id_event && eventModel.getEventData(id_event)) {
    return await responseHandler(QRCode.generateQR(id_event), res);
  }

  return res.status(500).json({
    errors: ["Event not found"],
    data: [eventModel.getEventData(id_event), id_event],
  });
};

module.exports = {
  generateQR,
};
