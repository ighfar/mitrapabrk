const materialModel = require("../models/material");
const { upload } = require("../helpers/uploader");
const responseHandler = require("../helpers/responseHandler");
const add = async (req, res) => {
  const pathToUpload = __dirname + "/../uploads/materials";
  try {
    const photo = await upload({
      type: "image",
      file: req.files.photo,
      path: pathToUpload,
      bucket: "mitrapabrik/materials",
    });

    req.body.photo = photo.fileName;

    return await responseHandler(materialModel.addMaterial(req.body), res);
  } catch (err) {
    return await responseHandler(err, res);
  }
};

module.exports = {
  add,
};
