const responseHandler = require("../helpers/responseHandler");
const { upload } = require("../helpers/uploader");
const projectModel = require("../models/project");
const materialModel = require("../models/material");
const AUTH = require("../auth");
const path = require("path");

const add = async (req, res) => {
  const pathToUpload = __dirname + "/../uploads/projects";
  const pathToUploadMaterial = path.join(__dirname, "../", "uploads", "materials");
  let token = req.header("auth-token");
  let auth = AUTH.authUserToken(token);

  let result = {};

  try {

    const checkName = await projectModel.getProjectByName(req.body.project_name);

    if (checkName) {

      result.status = 500;
      result.errors = ['project_name already exist.'];

      return res.status(result.status).send(result);
    }

    const photo = await upload({
      type: "image",
      file: req.files.project_photo,
      path: pathToUpload,
      bucket: "mitrapabrik/projects",
    });

    const dataProject = {
      project_deadline: req.body.project_deadline,
      person_name: req.body.person_name,
      person_phone: req.body.person_phone,
      project_name: req.body.project_name,
      project_address: req.body.project_address,
      project_city: req.body.project_city,
      project_note: req.body.project_note,
      project_status: req.body.project_status,
      project_photo: photo.fileName,
      user_id: auth.user.id_user
    };

    const responseProject = await projectModel.addProject(dataProject);

    const photoMaterial = req.files.photo;

    const dataMaterial = req.body.material;

   

    dataMaterial.forEach(async (element, key) => {

      const uploadMaterial = await upload({
        type: "image",
        file: photoMaterial[key],
        path: pathToUploadMaterial,
        bucket: "mitrapabrik/materials",
      });

      const dataMaterial = JSON.parse(element);
      dataMaterial.photo = uploadMaterial.fileName;
      dataMaterial.project_id = responseProject.id;
      console.log(dataMaterial);
      const resultMaterial = await materialModel.addMaterial(dataMaterial);
      console.log(resultMaterial);
    });

    result.status = 200;
    result.message = 'Material inquiry has been added';

  } catch (err) {
    return await responseHandler(err, res);
  }

  return res.status(result.status).send(result);
};

const list = async (req, res) => {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let result = {};

  result.status = 200;
  result.data = await projectModel.listProjects(search, orderBy, orderMethod, perPage, page);
  const totalRows = await projectModel.getTotalProjects(search);
  result.data.totalRows = totalRows[0].total;
  return res.status(result.status).send(result);
};

const reply = async (req, res) => {
  return await responseHandler(projectModel.replyProject(req.body), res);
};

const updateStatus = async (req, res) => {
  return await responseHandler(
    projectModel.updateProject({
      id: req.params.id,
      status: req.body.status,
    }),
    res
  );
};

const deleteProject = async (req, res) => {

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  const Id = req.body.id;

  try {
    const project = await projectModel.getProjectByIdFirst(Id);
    if (project) {
      const projectId = project.id;
      await materialModel.deleteMaterialByProject(projectId);
      await projectModel.deleteProject(projectId);
    }
    result.status = 200;

  } catch (error) {
    console.error(error);
    result.status = 500;
    result.error = error.message;
  }

  return res.status(result.status).send(result);
};

module.exports = {
  add,
  list,
  reply,
  updateStatus,
  deleteProject
};
