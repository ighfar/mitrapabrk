const responseHandler = require("../helpers/responseHandler");
const replyModel = require("../models/reply");

const add = async (req, res) => {
  return await responseHandler(replyModel.addReply(req.body), res);
};

const get = async (req, res) => {
  return await responseHandler(replyModel.listReplies(req.params.id), res);
};

const update = async (req, res) => {
  return await responseHandler(
    replyModel.updateReply({
      id: req.params.id,
      message: req.body.message,
    }),
    res
  );
};

module.exports = {
  add,
  get,
  update,
};
