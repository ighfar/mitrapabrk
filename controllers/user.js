const responseHandler = require("../helpers/responseHandler");
const userService = require("../services/user");

const verify = async (req, res) => {
  const token = req.body.hash;
  return await responseHandler(userService.verifyEmail(token), res);
};

module.exports = {
  verify,
};
