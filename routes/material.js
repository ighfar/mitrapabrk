const express = require("express");
const router = express.Router();
const validation = require("../validations/material");
const controller = require("../controllers/material");
const { roles } = require("../middlewares/authorizations/roleBased");

//router.use(roles(["Prinsipal"]));

router.post("/", validation.add, controller.add);

module.exports = router;
