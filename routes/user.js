require("dotenv").config();
const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
const sharp = require("sharp");
const AWS = require("aws-sdk");
const DB = require("../models/index");
const axios = require('axios');

const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");
const md5 = require("crypto-js/md5");
const { v1: uuidv1 } = require("uuid");

const plugins = require("../plugins");
const AUTH = require("../auth");
const userModel = require("../models/user");
const projectsModel = require("../models/project");
const materialModel = require("../models/material");
const productModel = require("../models/products");
const noticeModel = require("../models/notice");
const searchModel = require("../models/search");
const validations = require("../validations/user");
const controller = require("../controllers/user");
const { randomString } = require("../helpers/randomString");

const { sendVerificationEmail, sendEmailResetPassword } = require("../helpers/email");

const IMG_MIME_TYPE = ["image/png", "image/jpeg", "image/jpg"];

const spacesEndpoint = new AWS.Endpoint("sgp1.digitaloceanspaces.com");
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.DO_SPACES_KEY,
  secretAccessKey: process.env.DO_SPACES_SECRET,
});
const CryptoJS = require("crypto-js/aes");

let readHTMLFile = function (path, callback) {
  fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
    if (err) callback(err);
    else callback(null, html);
  });
};

function removeTempFile(file_dir) {
  let files = fs.readdirSync(path.join(__dirname, "../", "uploads", file_dir));
  files.forEach((file) => {
    if (file.split(".")[1] != "webp") {
      console.log(
        "removing: " + path.join(__dirname, "../", "uploads", file_dir, file)
      );
      fs.unlinkSync(path.join(__dirname, "../", "uploads", file_dir, file));
    }
  });
}

router.post("/check-token", async function (req, res) {
  let result = AUTH.authUserToken(req.header("auth-token"));
  res.status(result.status).send(result);
});

router.post("/admin/check-token", async function (req, res) {
  let result = AUTH.authAdminToken(req.header("auth-token"));
  res.status(result.status).send(result);
});

async function registerProtocols(req, auth, body) {
  let result = { status: 401 };
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      result.errors = errors.array();
    } else {
      result.status = 200;
      result.data = await userModel.updatedataDiri(body, auth.id_user);
      result.msg = "OK";
    }
  } catch (error) {
    console.log(error);
  }
  return result;
}
router.get('/paginate', (req, res) => {
  const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 0;
  const page = req.query.page ? parseInt(req.query.page) : 0;
  controller.getUsers(pageSize, page).then(data => res.json(data));
});
router.get("/", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await userModel.getAllCustomers(search, orderBy, orderMethod, perPage, page);
    const totalRows = await userModel.getTotalAllCustomers(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;

  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/principal", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await userModel.getAllPrincipal(search, orderBy, orderMethod, perPage, page);
    const totalRows = await userModel.getTotalAllPrincipal(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;

  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.put("/principal", [
  body("id_user").notEmpty().isNumeric().trim().escape(),
  body("youtube").notEmpty(),
], async function (req, res) {

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    const data = {
      youtube: req.body.youtube
    };
    result.data = await userModel.updatedataDiri(data, req.body.id_user);
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/principal/brand", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let result = {};

  try {
    result.data = await userModel.getAllPrincipalBrand(search, orderBy, orderMethod, perPage, page);
    const totalRows = await userModel.getTotalAllPrincipalBrand(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;

  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/partners", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await userModel.getAllPartners();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});
router.get("/company_logo", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await userModel.getPartnerFoto();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/prinsipal", async function (req, res) {
  let result = { status: 401, total: 0 };
  try {
    result.data = await userModel.getAllPrinsipal();
    result.total = await userModel.getCountPrinsipal();
    result.status = 200;
  } catch (error) {
    console.error(error);
    res.status(500).send({ status: 500, errors: error.message });
  }
  res.status(result.status).send(result);
});

router.get("/material",
  [body("project_id").isNumeric().trim().escape()],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    try {
      result.data = await userModel.getAllMaterials(req.body.project_id);
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
    res.status(result.status).send(result);
  });

router.put(
  "/prinsipal/status",
  [
    body("id_user").isNumeric().trim().escape(),
    body("status").isIn(["0", "1"]),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          let id_user = req.body.id_user;
          delete req.body["id_user"];
          result.data = await userModel.editUser(id_user, req.body.status);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.delete(
  "/",
  [body("id_user").isNumeric().trim().escape()],
  async function (req, res) {

    let auth = AUTH.authAdminToken(req.header("auth-token"));

    let result = {};

    if (auth.status != 200) {
      return res.status(auth.status).send(auth);
    }

    const userId = req.body.id_user;

    try {
      const project = await projectsModel.getUserProjectsFirst(userId);
      if (project) {
        const projectId = project.id;
        await materialModel.deleteMaterialByProject(projectId);
        await projectsModel.deleteProject(projectId);
      }
      await productModel.deleteProductByPartner(userId);
      result.data = await userModel.deleteUser(userId);
      result.status = 200;

    } catch (error) {
      console.log(error);
    }

    res.status(result.status).send(result);
  }
);

router.delete(
  "/material",
  [body("id").isNumeric().trim().escape()],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          result.data = await userModel.deleteMaterial(req.body.id);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.get("/project",
  [body("id").isNumeric().trim().escape()],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    try {
      result.data = await projectsModel.getProjectDetail(req.body.id);
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
    res.status(result.status).send(result);
  });

router.delete(
  "/project",
  [body("id").isNumeric().trim().escape()],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          result.data = await projectModel.deleteProject(req.body.id);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.get("/partner/:id_partner", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    let fetchData = await userModel.getPartnerData(req.params.id_partner);
    if (fetchData.length > 0) {
      result.data = fetchData;
      result.status = 200;
    } else {
      result.data = [];
      result.msg = "id partner tidak ditemukan.";
    }
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.post(
  "/register/validate/1",
  [
    body("nama_depan")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .customSanitizer((value) => plugins.toTitleCase(value))
      .withMessage("nama depan tidak valid")
      .trim()
      .escape(),
    body("nama_belakang")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .customSanitizer((value) => plugins.toTitleCase(value))
      .withMessage("nama belakang tidak valid")
      .trim()
      .escape(),
    body("email")
      .isEmail({ gmail_remove_dots: false })
      .withMessage("format email tidak valid. contoh: jondoe@gmail.com"),
    body("password")
      .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[ !"#$%&\'()*+,-.\/\\:;<=>?@[\]^_`{|}~]).{6,}$/)
      .withMessage(
        "Minimal 1 huruf kapital, panjang password minimal 6 karakter, mengandung minimal 1 angka dan special character."
      ),
    body("cpassword")
      .custom((value, { req }) => value === req.body.password)
      .withMessage("password tidak sama"),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      try {
        delete req.body["cpassword"];
        let tempBody = {
          nama_depan: req.body.nama_depan,
          nama_belakang: req.body.nama_belakang,
          email: req.body.email,
          password: req.body.password,
          role: 'User',
        };
        let tryRegister = await userModel.register(tempBody);
        if (tryRegister.status) {
          let checkLogin = await userModel.login(
            req.body.email,
            req.body.password
          );

          delete checkLogin["status"];
          result.token = AUTH.createUserToken(checkLogin);
          const hash = AUTH.createEmailToken(tempBody.email);
          sendVerificationEmail(hash, {
            email: tempBody.email,
            nama_depan: tempBody.nama_depan,
            nama_belakang: tempBody.nama_belakang,
          });
          result.data = checkLogin;
          result.status = 200;
        } else {
          result.errors.push(tryRegister.err);
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.post(
  "/register",
  [
    body("nama_lengkap")
      .customSanitizer((value) => plugins.toTitleCase(value))
      .trim()
      .escape(),
    body("gender")
      .notEmpty()
      .withMessage("Jenis kelamin tidak boleh kosong.")
      .trim()
      .escape(),
    body("tanggal_lahir")
      .notEmpty()
      .withMessage("Tanggal lahir tidak boleh kosong.")
      .trim()
      .escape(),
    body("alamat")
      .notEmpty()
      .withMessage("Alamat tidak boleh kosong.")
      .trim()
      .escape(),
    body("asal_kota")
      .notEmpty()
      .withMessage("Kota tidak boleh kosong.")
      .trim()
      .escape(),
    body("no_hp")
      .notEmpty()
      .withMessage("Nomer telepone tidak boleh kosong.")
      .trim()
      .escape(),
    body("email")
      .notEmpty()
      .withMessage("Email tidak boleh kosong.")
      .isEmail({ gmail_remove_dots: false })
      .withMessage("format email tidak valid. contoh: jondoe@gmail.com"),
    body("profesi")
      .notEmpty()
      .withMessage("Profesi tidak boleh kosong.")
      .trim()
      .escape(),
    body("nama_perusahaan")
      .notEmpty()
      .withMessage("Perusahaan tidak boleh kosong.")
      .trim()
      .escape(),
    body("nomor_stra")
      .trim()
      .escape(),
    body("password")
      .notEmpty()
      .withMessage('Kata sandi tidak boleh kosong')
      .matches(/^(?=.*\d)(?=.*[a-zA-Z]).{8,30}$/)
      .withMessage('Harus kombinasi huruf dan angka')
      .isLength({ min: 8 })
      .withMessage('Minimal 8 karakter')
      .isLength({ max: 30 })
      .withMessage('Maksimal 30 karakter'),
    body("cpassword")
      .notEmpty().withMessage("Ulang kata sandi tidak boleh kosong.")
      .matches(/\d/)
      .withMessage('Harus kombinasi huruf dan angka')
      .isLength({ min: 8 })
      .withMessage('Minimal 8 karakter')
      .isLength({ max: 30 })
      .withMessage('Maksimal 30 karakter')
      .custom((value, { req }) => value === req.body.password)
      .withMessage("Kata sandi tidak sama"),
    body("minat")
      .notEmpty()
      .withMessage("Minat tidak boleh kosong.")
      .trim()
      .escape(),
    body("information_about")
      .notEmpty()
      .withMessage("Tahu Mitrapabrik dari mana tidak boleh kosong.")
      .trim()
      .escape(),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    } else {
      try {
        delete req.body["cpassword"];
        let tempBody = {
          nama_lengkap: req.body.nama_lengkap,
          gender: req.body.gender,
          tanggal_lahir: req.body.tanggal_lahir,
          alamat: req.body.alamat,
          asal_kota: req.body.asal_kota,
          no_hp: req.body.no_hp,
          profesi: req.body.profesi,
          nama_perusahaan: req.body.nama_perusahaan,
          nomor_stra: req.body.nomor_stra,
          minat: req.body.minat,
          information_about: req.body.information_about,
          email: req.body.email,
          password: req.body.password,
          role: 'User',
        };
        let tryRegister = await userModel.register(tempBody);
        if (tryRegister.status) {
          let checkLogin = await userModel.login(
            req.body.email,
            req.body.password
          );

          delete checkLogin["status"];
          result.token = AUTH.createUserToken(checkLogin);
          const hash = AUTH.createEmailToken(tempBody.email);
          sendVerificationEmail(hash, {
            email: tempBody.email,
            nama_lengkap: tempBody.nama_lengkap
          });
          result.data = checkLogin;
          result.status = 200;
        } else {
          result.errors.push(tryRegister.err);
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.post(
  "/register/principal",
  [
    body("nama_perusahaan")
      .notEmpty()
      .withMessage("Nama perusahaan tidak boleh kosong")
      .trim()
      .escape(),
    body("nama_direktur").notEmpty()
      .withMessage("Nama direktur tidak boleh kosong").trim().escape(),
    body("alamat_perusahaan").notEmpty()
      .withMessage("Alamat perusahaan tidak boleh kosong").trim().escape(),
    body("nomer_telepone")
      .notEmpty()
      .withMessage("Nomer telephone tidak boleh kosong")
      .trim()
      .escape(),
    body("nomer_fax")
      .notEmpty()
      .withMessage("Nomer fax tidak boleh kosong")
      .trim()
      .escape(),
    body("email_perusahaan")
      .notEmpty()
      .withMessage("Email 1 tidak boleh kosong")
      .trim()
      .escape(),
    body("email_secondary")
      .notEmpty()
      .withMessage("Email 2 tidak boleh kosong")
      .trim()
      .escape(),
    body("nomer_tdp")
      .notEmpty()
      .withMessage("Nomer TDP tidak boleh kosong")
      .trim()
      .escape(),
    body("tipe_barang")
      .notEmpty()
      .withMessage("Jenis produk tidak boleh kosong")
      .trim()
      .escape(),
    body("minat")
      .notEmpty()
      .withMessage("Minat tidak boleh kosong")
      .trim()
      .escape(),
    body("information_about")
      .notEmpty()
      .withMessage("Tahu Mitrapabrik dari mana tidak boleh kosong")
      .trim()
      .escape(),
    body("tujuan")
      .notEmpty()
      .withMessage("Tujuan join tidak boleh kosong")
      .trim()
      .escape(),
  ],
  async function (req, res) {
    let result = { status: 401 };
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    if (auth.status != 200) result = auth;
    else {
      let tmpBody = {
        nama_perusahaan: req.body.nama_perusahaan,
        nama_direktur: req.body.nama_direktur,
        alamat_perusahaan: req.body.alamat_perusahaan,
        nomer_telepone: req.body.nomer_telepone,
        nomer_fax: req.body.nomer_fax,
        email_perusahaan: req.body.email_perusahaan,
        email_secondary: req.body.email_secondary,
        nomer_tdp: req.body.nomer_tdp,
        tipe_barang: req.body.tipe_barang,
        minat: req.body.minat,
        information_about: req.body.information_about,
        tujuan: req.body.tujuan,
      };
      result = await registerProtocols(req, auth.user, tmpBody);
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/register/validate/2",
  [
    body("no_hp").matches(/^.[0-9]{7,14}$/),
    body("asal_kota")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .customSanitizer((value) => plugins.toTitleCase(value))
      .trim()
      .escape(),
    body("profesi").notEmpty().trim().escape(),
    body("tujuan").notEmpty().trim().escape(),
    body("minat")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .trim()
      .escape(),
    // body('foto_profil').notEmpty().trim().escape(),
  ],
  async function (req, res) {
    let result = { status: 401 };
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    if (auth.status != 200) result = auth;
    else {
      let tmpBody = {
        no_hp: req.body.no_hp,
        asal_kota: req.body.asal_kota,
        profesi: req.body.profesi,
        tujuan: req.body.tujuan,
        minat: req.body.minat,
        foto_profil: req.body.foto_profil,
      };
      result = await registerProtocols(req, auth.user, tmpBody);
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/register/validate/3",
  [
    body("nama_perusahaan")
      .trim()
      .escape(),
    body("kategori_perusahaan").notEmpty().trim().escape(),
    body("alamat_perusahaan").notEmpty().trim().escape(),
    // body('foto_perusahaan').notEmpty().trim().escape(),
  ],
  async function (req, res) {
    let result = { status: 401 };
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    if (auth.status != 200) result = auth;
    else {
      let tmpBody = {
        nama_perusahaan: req.body.nama_perusahaan,
        kategori_perusahaan: req.body.kategori_perusahaan,
        alamat_perusahaan: req.body.alamat_perusahaan,
        foto_perusahaan: req.body.foto_perusahaan,
      };
      result = await registerProtocols(req, auth.user, tmpBody);
    }
    res.status(result.status).send(result);
  }
);

router.post(
  "/register/quick",
  [
    body("nama_depan")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .customSanitizer((value) => plugins.toTitleCase(value))
      .withMessage("nama depan tidak valid")
      .trim()
      .escape(),
    body("nama_belakang")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .customSanitizer((value) => plugins.toTitleCase(value))
      .withMessage("nama belakang tidak valid")
      .trim()
      .escape(),
    body("email")
      .isEmail({ gmail_remove_dots: false })
      .withMessage("format email tidak valid. contoh: jondoe@gmail.com"),
    body("nama_perusahaan")
      .trim()
      .escape(),
    body("alamat_perusahaan").notEmpty().trim().escape(),
    body("no_hp").matches(/^.[0-9]{7,14}$/),
    body("minat").notEmpty().trim().escape(),
    body("role")
      .isIn(["Prinsipal", "User"])
      .withMessage("pilih salah satu role"),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      try {
        let tempBody = {
          nama_depan: req.body.nama_depan,
          nama_belakang: req.body.nama_belakang,
          email: req.body.email,
          nama_perusahaan: req.body.nama_perusahaan,
          alamat_perusahaan: req.body.alamat_perusahaan,
          no_hp: req.body.no_hp,
          minat: req.body.minat,
          password: "asd1234567",
          role: req.body.role,
        };
        let tryRegister = await userModel.register(tempBody);
        if (tryRegister.status) {
          const hash = AUTH.createEmailToken(tempBody.email);
          sendVerificationEmail(hash, {
            email: tempBody.email,
            nama_depan: tempBody.nama_depan,
            nama_belakang: tempBody.nama_belakang,
          });
          result.data = tryRegister;
          result.status = 200;
        } else {
          result.errors.push(tryRegister.err);
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.post(
  "/email/send",
  [

    body("email")
      .isEmail({ gmail_remove_dots: false })
      .withMessage("format email tidak valid. contoh: jondoe@gmail.com"),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      try {
        let userData = await userModel.getUserByEmail(req.body.email);

        if (userData) {
          const randomPassword = Math.floor(100000000 + Math.random() * 900000000);
          const idUser = userData.id_user;

          const update = userModel.updatePassword(idUser, randomPassword);

          let tempBody = {
            email: req.body.email,
            nama_lengkap: userData.nama_lengkap,
            password_nohash: randomPassword,
          };

          const hash = AUTH.createEmailToken(tempBody.email);
          sendEmailResetPassword(hash, {
            email: tempBody.email,
            nama_lengkap: tempBody.nama_lengkap,
            password_nohash: tempBody.password_nohash,
          });

        }
        result.status = 200;

      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  });

router.get("/datadiri", async function (req, res) {
  let result = { status: 401, errors: [] };
  let auth = AUTH.authUserToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      result.data = await userModel.getUserData(auth.user.email);
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.get("/principal/profile", async function (req, res) {
  let result = { status: 401, errors: [] };
  let auth = AUTH.authUserToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      result.data = await userModel.getUserData(auth.user.email);
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.get("/product/tipe", async function (req, res) {
  let result = { status: 401, errors: [] };
  let auth = AUTH.authUserToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      result.data = await userModel.listTypeByUser(auth.user.email);
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});
router.put(
  "/datadiri",
  [
    body("nama_lengkap")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .customSanitizer((value) => plugins.toTitleCase(value))
      .withMessage("Nama tidak valid")
      .trim()
      .escape(),
    body("gender")
      .notEmpty()
      .withMessage("Jenis kelamin tidak boleh kosong.")
      .trim()
      .escape(),
    body("tanggal_lahir")
      .notEmpty()
      .withMessage("Tanggal lahir tidak boleh kosong.")
      .trim()
      .escape(),
    body("alamat")
      .notEmpty()
      .withMessage("Alamat tidak boleh kosong.")
      .trim()
      .escape(),
    body("asal_kota")
      .notEmpty()
      .withMessage("Kota tidak boleh kosong.")
      .trim()
      .escape(),
    body("no_hp")
      .notEmpty()
      .withMessage("Nomer telepone tidak boleh kosong.")
      .matches(/^.[0-9]{7,14}$/)
      .withMessage("Nomer telepone tidak valid.")
      .trim()
      .escape(),
    body("email")
      .notEmpty()
      .withMessage("Email tidak boleh kosong.")
      .isEmail({ gmail_remove_dots: false })
      .withMessage("format email tidak valid. contoh: jondoe@gmail.com"),
    body("profesi")
      .notEmpty()
      .withMessage("Profesi tidak boleh kosong.")
      .trim()
      .escape(),
    body("nama_perusahaan")
      .notEmpty()
      .withMessage("Perusahaan tidak boleh kosong.")
      .trim()
      .escape(),
    body("minat")
      .notEmpty()
      .withMessage("Minat tidak boleh kosong.")
      .trim()
      .escape(),
  ],
  async function (req, res) {
    let result = { status: 401 };
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    if (auth.status != 200) result = auth;
    else {
      let tmpBody = {
        nama_lengkap: req.body.nama_lengkap,
        gender: req.body.gender,
        tanggal_lahir: req.body.tanggal_lahir,
        alamat: req.body.alamat,
        asal_kota: req.body.asal_kota,
        no_hp: req.body.no_hp,
        profesi: req.body.profesi,
        nama_perusahaan: req.body.nama_perusahaan,
        minat: req.body.minat,
        email: req.body.email,
      };
      result = await registerProtocols(req, auth.user, tmpBody);
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/datadiri/perusahaan",
  [
    body("nama_perusahaan")
      .notEmpty()
      .withMessage("Nama perusahaan tidak boleh kosong")
      .trim()
      .escape(),
    body("nama_direktur").notEmpty()
      .withMessage("Nama direktur tidak boleh kosong").trim().escape(),
    body("alamat_perusahaan").notEmpty()
      .withMessage("Alamat perusahaan tidak boleh kosong").trim().escape(),
    body("nomer_telepone")
      .notEmpty()
      .withMessage("Nomer telephone tidak boleh kosong")
      .trim()
      .escape(),
    body("nomer_fax")
      .notEmpty()
      .withMessage("Nomer fax tidak boleh kosong")
      .trim()
      .escape(),
    body("email_perusahaan")
      .notEmpty()
      .withMessage("Email 1 tidak boleh kosong")
      .trim()
      .escape(),
    body("email_secondary")
      .notEmpty()
      .withMessage("Email 2 tidak boleh kosong")
      .trim()
      .escape(),
    body("nomer_tdp")
      .notEmpty()
      .withMessage("Nomer TDP tidak boleh kosong")
      .trim()
      .escape(),
    body("tipe_barang")
      .notEmpty()
      .withMessage("Jenis produk tidak boleh kosong")
      .trim()
      .escape(),
  ],
  async function (req, res) {
    let result = { status: 401 };
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    if (auth.status != 200) result = auth;
    else {
      let tmpBody = {
        nama_perusahaan: req.body.nama_perusahaan,
        nama_direktur: req.body.nama_direktur,
        alamat_perusahaan: req.body.alamat_perusahaan,
        asal_kota: req.body.asal_kota,
        nomer_telepone: req.body.nomer_telepone,
        nomer_fax: req.body.nomer_fax,
        email_perusahaan: req.body.email_perusahaan,
        email_secondary: req.body.email_secondary,
        nomer_tdp: req.body.nomer_tdp,
        tipe_barang: req.body.tipe_barang,
      };
      result = await registerProtocols(req, auth.user, tmpBody);
    }
    res.status(result.status).send(result);
  }
);

router.post("/datadiri/gallery", async function (req, res) {
  let result = { status: 401 };
  let token = req.header("auth-token");
  let auth = AUTH.authUserToken(token);
  if (auth.status != 200) result = auth;
  else {
    try {
      let checkUserGallery = await userModel.getUserGalleries(
        auth.user.id_user
      );
      let tmpGallery =
        checkUserGallery[0].gallery != null
          ? checkUserGallery[0].gallery.split(",")
          : [];
      if (tmpGallery.length < 10) {
        if (req.files.gambar instanceof Array) {
          console.log("multiple");
          req.files.gambar.forEach(async (foto, idx) => {
            let extension = foto.mimetype.split("/")[1];
            let newName = `${auth.user.id_user}_${tmpGallery.length}.${extension}`;
            console.log(newName);
            let mimeValidation = false,
              dir = "",
              key;
            if (IMG_MIME_TYPE.includes(foto.mimetype)) {
              mimeValidation = true;
              dir = "gallery";
              key = uuidv1();
              tmpGallery.push(`${key}`);
            }
            if (mimeValidation) {
              await foto.mv(
                path.join(__dirname, "../", "uploads", dir, newName)
              );
              let compressResult = await sharp(
                path.join(__dirname, "../", "uploads", dir, newName)
              )
                .webp({ quality: 80 })
                .toBuffer();
              fs.writeFileSync(
                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                compressResult
              );

              await fs.readFile(
                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                (err, finalFile) => {
                  var params = {
                    Bucket: `mitrapabrik/${dir}`,
                    Key: key,
                    Body: finalFile,
                    ACL: "public-read",
                    ContentType: "image/webp",
                    ContentEncoding: "base64",
                  };
                  s3.putObject(params, function (err, data) {
                    if (err) {
                      console.log(err, err.stack);
                    } else {
                      console.log(data);
                    }
                  });
                }
              );
              removeTempFile(dir);
            } else {
              console.log("mime invalid");
            }
          });
        } else {
          console.log("single");
          let foto = req.files.gambar;
          let extension = foto.mimetype.split("/")[1];
          let newName = `${auth.user.id_user}_${tmpGallery.length}.${extension}`;
          console.log(newName);
          let mimeValidation = false,
            dir = "",
            key;
          if (IMG_MIME_TYPE.includes(foto.mimetype)) {
            mimeValidation = true;
            dir = "gallery";
            key = uuidv1();
            tmpGallery.push(`${key}`);
          }
          if (mimeValidation) {
            await foto.mv(path.join(__dirname, "../", "uploads", dir, newName));
            let compressResult = await sharp(
              path.join(__dirname, "../", "uploads", dir, newName)
            )
              .webp({ quality: 80 })
              .toBuffer();
            fs.writeFileSync(
              path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
              compressResult
            );

            await fs.readFile(
              path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
              (err, finalFile) => {
                var params = {
                  Bucket: `mitrapabrik/${dir}`,
                  Key: key,
                  Body: finalFile,
                  ACL: "public-read",
                  ContentType: "image/webp",
                  ContentEncoding: "base64",
                };
                s3.putObject(params, function (err, data) {
                  if (err) {
                    console.log(err, err.stack);
                  } else {
                    console.log(data);
                  }
                });
              }
            );
            removeTempFile(dir);
          } else {
            console.log("mime invalid");
          }
        }
        await userModel.updatedataDiri(
          { gallery: tmpGallery.toString() },
          auth.user.id_user
        );
        result.status = 200;
      } else {
        result.msg = "maksimum gallery hanya 10";
      }
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.delete("/datadiri/gallery/:idx", async function (req, res) {
  let result = { status: 401, errors: [] };
  let auth = AUTH.authUserToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      let currentGalleries = await userModel.getUserGalleries(
        auth.user.id_user
      );
      if (currentGalleries.length > 0) {
        let galleries = currentGalleries[0].gallery.split(",");
        let deleted = galleries.splice(req.params.idx, 1);

        let files = fs.readdirSync(
          path.join(__dirname, "../", "uploads", "gallery")
        );
        files.forEach((file) => {
          if (file == deleted) {
            fs.unlinkSync(
              path.join(__dirname, "../", "uploads", "gallery", file)
            );
          }
        });
        let final = galleries.toString();
        if (final == "") final = null;
        await userModel.updateGalleries(auth.user.id_user, final);
        result.status = 200;
        result.msg = "gallery berhasil dihapus";
      } else {
        result.msg = "gambar tidak ditemukan";
      }
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.post(
  "/login",
  [
    body("email").isEmail({ gmail_remove_dots: false }),
    body("password"),
    // Tidak perlu memilih role saat login, karna role sudah otomatis terpilih saat register
    // saat login role akan diambil dari database
    // body("role")
    //   .isIn(["Prinsipal", "Partner"])
    //   .withMessage("pilih salah satu role")
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      try {
        let checkLogin = await userModel.login(
          req.body.email,
          req.body.password
        );

        if (checkLogin.status) {
          result.data = checkLogin;
          delete checkLogin["status"];
          result.token = AUTH.createUserToken(checkLogin);
          result.status = 200;
        } else {
          result.errors.push(checkLogin.err);
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.post(
  "/admin/login",
  [
    body("id_admin").notEmpty().trim().escape(),
    body("password").matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      try {
        let checkLogin = await userModel.adminLogin(
          req.body.id_admin,
          req.body.password
        );
        if (checkLogin.status) {
          result.data = checkLogin;
          delete checkLogin["status"];
          result.token = AUTH.createAdminToken(checkLogin);
          result.status = 200;
        } else {
          result.errors.push(checkLogin.err);
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.post(
  "/admin/register",
  [
    body("id_admin").notEmpty().trim().escape(),
    body("password").matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/),
    body("cpassword").custom((value, { req }) => value === req.body.password),
    body("nama")
      .matches(/^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9 ])?[a-zA-Z0-9]*)*$/)
      .customSanitizer((value) => plugins.toTitleCase(value))
      .trim()
      .escape(),
    body("level").isNumeric().trim().escape(),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      try {
        delete req.body["cpassword"];
        let tryRegister = await userModel.adminRegister(req.body);
        if (tryRegister.status) {
          result.data = tryRegister.data;
          result.status = 200;
        } else {
          result.errors.push(tryRegister.err);
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/password/change",
  [
    body("oldpass").notEmpty().withMessage("password tidak boleh kosong"),
    body("newpass")
      .notEmpty()
      .withMessage('Kata sandi tidak boleh kosong')
      .matches(/^(?=.*\d)(?=.*[a-zA-Z]).{8,30}$/)
      .withMessage('Harus kombinasi huruf dan angka')
      .isLength({ min: 8 })
      .withMessage('Minimal 8 karakter')
      .isLength({ max: 30 })
      .withMessage('Maksimal 30 karakter'),
    body("cnewpass")
      .custom((value, { req }) => value === req.body.newpass)
      .withMessage("konfirmasi passsword tidak sama"),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    if (auth.status != 200) {
      result = auth;
    } else {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(401).json({ errors: errors.array() });
      } else {
        let oldpass = req.body.oldpass;
        let newpass = req.body.newpass;
        try {
          let getPass = await userModel.getUserPassword(auth.user.email);
          if (userModel.checkPassword(getPass[0].password, oldpass)) {
            result.data = await userModel.updatePassword(
              auth.user.id_user,
              newpass
            );
            result.status = 200;
            result.msg = "ganti password berhasil";
          } else {
            result.errors.push({
              param: "oldpass",
              msg: "password tidak valid",
            });
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/password/reset",
  [
    body("newpass").matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/),
    body("cnewpass").custom((value, { req }) => value === req.body.newpass),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    if (auth.status != 200) {
      result = auth;
    } else {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(401).json({ errors: errors.array() });
      } else {
        let newpass = req.body.newpass;
        try {
          result.data = await userModel.updatePassword(
            auth.user.id_user,
            newpass
          );
          result.status = 200;
          result.msg = "ganti password berhasil";
        } catch (error) {
          console.log(error);
        }
      }
    }
    res.status(result.status).send(result);
  }
);
router.put(
  "/password/set",
  [
    body("t").matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/),
    body("newpass").matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/),
    body("cnewpass").custom((value, { req }) => value === req.body.newpass),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      let newpass = req.body.newpass;
      try {
        result.data = await userModel.forgetPassword(
          newpass
        );
        result.status = 200;
        result.msg = "ganti password berhasil";
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }


);

router.post("/upload", async function (req, res) {
  let result = { status: 401, errors: [] };
  let token = req.header("auth-token");
  let auth = AUTH.authUserToken(token);
  if (auth.status != 200) result = auth;
  else {
    if (!req.files || Object.keys(req.files).length === 0) {
      result.msg = "Tidak ada file yang diupload.";
    } else {
      try {
        let foto = req.files.foto;
        let extension = foto.mimetype.split("/")[1];
        let newName = `${auth.user.id_user}.${extension}`;
        let mimeValidation = false,
          dir = "",
          key;
        let fileName = '';
        if (IMG_MIME_TYPE.includes(foto.mimetype) && req.body.idx < 2) {
          mimeValidation = true;
          dir = req.body.idx == 0 ? "profile" : "company_logo";
          key = uuidv1().toString();
        }
        if (["application/pdf"].includes(foto.mimetype) && req.body.idx == 2) {
          mimeValidation = true;
          dir = "pdfs";
          key = uuidv1().toString();
        }
        if (mimeValidation) {
          await foto.mv(path.join(__dirname, "../", "uploads", dir, newName));
          let compressResult = await sharp(
            path.join(__dirname, "../", "uploads", dir, newName)
          )
            .webp({ quality: 80 })
            .toBuffer();
          fs.writeFileSync(
            path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
            compressResult
          );

          await fs.readFile(
            path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
            (err, finalFile) => {
              var params = {
                Bucket: `mitrapabrik/${dir}`,
                Key: key,
                Body: finalFile,
                ACL: "public-read",
                ContentType: "image/webp",
                ContentEncoding: "base64",
              };
              s3.upload(params).promise();
            }
          );
          removeTempFile(dir);

          result.data = req.body.idx == 0 ? await userModel.updateFoto(auth.user.id_user, key) : await userModel.updateFotoPerusahaan(auth.user.id_user, key);
          result.profile = key;
          result.status = 200;
          result.msg = "upload file berhasil!";
        } else {
          result.errors.push({
            msg: "Invalid Value",
            param: "foto",
            location: "body",
          });
        }
      } catch (error) {
        console.log(error);
        result.errors = error.message;
        result.msg =
          "Mohon maaf, terjadi kesalahan. Silahkan coba beberapa saat lagi.";
      }
    }
  }
  res.status(result.status).send(result);
});

router.post("/upload/pdf", async function (req, res) {
  let result = { status: 401, errors: [] };
  let token = req.header("auth-token");
  let auth = AUTH.authUserToken(token);
  if (auth.status != 200) result = auth;
  else {
    if (!req.files || Object.keys(req.files).length === 0) {
      result.msg = "Tidak ada file yang diupload.";
    } else {
      try {
        let foto = req.files.foto;
        let mimeValidation = false,
          dir = "pdfs",
          key;

        const fileName = req.files.foto.name.split(".")[0].replace(/ /g, "_");
        const currentStamp = new Date().getTime().toString().substr(-8);
        const changeName = 'portofolio_' + fileName.toLowerCase() + currentStamp;

        if (["application/pdf"].includes(foto.mimetype) && req.body.idx == 2) {
          mimeValidation = true;
          key = changeName;
        }
        if (mimeValidation) {
          await foto.mv(
            path.join(__dirname, "../", "uploads", dir, `${key}.pdf`)
          );
          fs.readFile(
            path.join(__dirname, "../", "uploads", dir, `${key}.pdf`),
            (err, finalFile) => {
              var params = {
                Bucket: `mitrapabrik/${dir}`,
                Key: key,
                Body: finalFile,
                ACL: "public-read",
                ContentType: "application/pdf",
                ContentEncoding: "base64",
              };
              s3.putObject(params, function (err, data) {
                if (err) {
                  console.log(err, err.stack);
                } else {
                  console.log(data);
                }
              });
            }
          );

          removeTempFile(dir);

          await userModel.updatedataDiri(
            { portofolio: key },
            auth.user.id_user
          );
          result.status = 200;
          result.data = `${key}`;
          result.msg = "upload PDF berhasil!";
        } else {
          result.msg = "Mohon maaf, file tidak valid.";
        }
      } catch (error) {
        console.log(error);
        result.msg =
          "Mohon maaf, terjadi kesalahan. Silahkan coba beberapa saat lagi.";
      }
    }
  }
  res.status(result.status).send(result);
});

router.post("/send/email-verify", async function (req, res) {
  let result = { status: 401, errors: [] };
  let token = req.header("auth-token");
  let auth = AUTH.authUserToken(token);
  if (auth.status != 200) {
    result = auth;
    res.status(result.status).send(result);
  } else {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      let email = auth.user.email;
      let hash = AUTH.createEmailToken(email);
      let userData = await userModel.getUserData(email);
      let domain = "https://mitrapabrik.com";
      readHTMLFile(
        path.join(__dirname, "../", "asset", "verify-body.html"),
        async function (err, html) {
          let template = handlebars.compile(html);
          let replacements = {
            verifyLink: `${domain}?t=${hash}`,
            nama_lengkap: userData[0].nama_lengkap
          };
          let htmlToSend = template(replacements);
          let sendEmail = await plugins.handleMailerAPI(
            "https://api.elasticemail.com/v4/emails/transactional",
            `Dear. ${plugins.decodeHtml(
              userData[0].nama_lengkap
            )}, Silahkan Verifikasi Email Anda`,
            htmlToSend,
            email
          );
          result.data = sendEmail;
          result.status = 200;
          res.status(result.status).send(result);
        }
      );
    }
  }
});

router.put("/verify", async function (req, res) {
  let result = { status: 401 };
  let token = req.header("auth-token");
  let auth = AUTH.authUserToken(token);
  if (auth.status != 200) {
    result = auth;
  } else {
    let hash = req.body.hash || "";
    if (hash.length > 0) {
      let checkHash = AUTH.verifyEmailToken(hash);
      if (checkHash.status === 1) {
        try {
          await userModel.verifyUserEmail(auth.user.email);
          let tokenData = await userModel.getUserData(auth.user.email);
          result.token = AUTH.createUserToken({
            id_user: tokenData[0].id_user,
            email: tokenData[0].email,
            nama_depan: tokenData[0].nama_depan,
            nama_belakang: tokenData[0].nama_belakang,
            role: tokenData[0].role,
            user_status: tokenData[0].status,
          });
          result.status = 200;
        } catch (error) {
          console.log(error);
        }
      } else {
        result.error = "hash invalid";
      }
    } else {
      result.msg = "hash not found";
    }
  }
  res.status(result.status).send(result);
});

router.put("/verify-email", validations.verify, controller.verify);

router.get("/notice", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await noticeModel.getAllNotice();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/omnisearch", async function (req, res) {
  let result = { status: 401, errors: [] };
  let promises = [];
  let search = req.query.search || "";
  try {
    promises.push(Promise.resolve(searchModel.searchArtikel(search)));
    promises.push(Promise.resolve(searchModel.searchEvent(search)));
    promises.push(Promise.resolve(searchModel.searchProduct(5, 0, search)));
    promises.push(Promise.resolve(searchModel.searchShowroom(search)));

    await Promise.all(promises).then((values) => {
      result.data = values;
      result.status = 200;
    });
  } finally {
    res.status(result.status).send(result);
  }
});

router.get("/generate-pocketbaseid", async function (req, res) {

  let result = { status: 401, errors: [] };

  try {

    let users = await userModel.getAllUsers();
    let collection = [];

    users.forEach((user) => {
      const key = randomString(15);
      const data = {
        id_user: user.id_user,
        pocketbase_id: key
      };
      collection.push(data);
    });

    await userModel.batchUpdate({ table: 'user', column: 'id_user' }, collection);

  } catch (err) {
    console.error(err);
    result.status = 500;
    result.errors = err.message;
    return res.status(result.status).send(result);
  }

  result.status = 200;

  return res.status(result.status).send(result);
});

router.get("/get/pocketbaseid", async function (req, res) {

  let result = { status: 401, errors: [] };

  try {

    let users = await userModel.getAllUsersNoticeNotCreate();
    users.forEach(async (user) => {
      user.id = randomString(15);
      postRequest = await axios({
        method: 'post',
        url: 'http://127.0.0.1:8090/api/collections/count_notice/records',
        headers: {
          'accept': "application/json",
          'content-type': "application/json",
        },
        data: user
      }).then((res) => userModel.editUser(user.userid, { pocketbase_id: user.id, notice_create: 1 }));
    });

    result.status = 200;
    result.data = users;
    result.errors = false

  } catch (err) {
    console.error(err);
    result.status = 500;
    result.errors = err.message;
    return res.status(result.status).send(result);
  }

  return res.status(result.status).send(result);
});

router.get("/all/userid", async function (req, res) {

  let result = { status: 401, errors: [] };

  try {

    let users = await userModel.getAllUserIds();
    result.status = 200;
    result.data = users;
    result.errors = false

  } catch (err) {
    console.error(err);
    result.status = 500;
    result.errors = err.message;
    return res.status(result.status).send(result);
  }

  return res.status(result.status).send(result);
});

router.get("/update/pocketbaseid/:id", async function (req, res) {

  let result = { status: 401, errors: [] };

  console.log('rr' + req.params.id);

  try {

    await userModel.editUser(req.params.id, { notice_create: 1 });
    result.status = 200;
    result.errors = false

  } catch (err) {
    console.error(err);
    result.status = 500;
    result.errors = err.message;
    return res.status(result.status).send(result);
  }

  return res.status(result.status).send(result);
});

// admin role can approve user status to 1
router.put("/approve", async function (req, res) {
  let result = { status: 401, errors: [] };
  let token = req.header("auth-token");
  let auth = AUTH.authAdminToken(token);
  if (auth.status != 200) {
    result = auth;
    res.status(result.status).send(result);
  } else {
    if (!req.body.id_user) {
      result.msg = "id_user not found";
    } else {
      try {
        await userModel.verifyUserId(req.body.id_user);
        result.status = 200;
        result.msg = "user approved";
      } catch (error) {
        console.log(error);
      }
    }
  }
  res.status(result.status).send(result);
});

router.post(
  "/newsletter",
  [

    body("email")
      .isEmail({ gmail_remove_dots: false })
      .withMessage("format email tidak valid. contoh: jondoe@gmail.com"),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(401).json({ errors: errors.array() });
    } else {
      try {

        let check = await userModel.checkNewsLetter(req.body.email);

        if (!check) {
          await userModel.addNewsLetter({ email: req.body.email });
        }

        result.status = 200;

      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  });

router.post("/crypto", [], async function (req, res) {
  let encryptedPass = CryptoJS.encrypt(
    'Demo123456',
    process.env.SECRET_KEY
  ).toString();
  res.status(200).send(encryptedPass);
});

module.exports = router;
