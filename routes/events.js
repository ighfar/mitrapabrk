require("dotenv").config();
const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
const AUTH = require("../auth");
const eventsModel = require("../models/events");
const noticeModel = require("../models/notice");
const { v1: uuidv1 } = require("uuid");
var QRCode = require("qrcode");
const plugins = require("../plugins");
const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");
const controller = require("../controllers/event");
const { roles } = require("../middlewares/authorizations/roleBased");
const { upload } = require("../helpers/uploader");

const IMAGE_MIME_TYPES = [
  "image/png",
  "image/jpg",
  "image/jpeg",
  "image/gif",
  "image/webp",
];

let readHTMLFile = function (path, callback) {
  fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
    if (err) callback(err);
    else callback(null, html);
  });
};

router.get("/", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await eventsModel.getAllEvents(search, orderBy, orderMethod, perPage, page);
    const totalRows = await eventsModel.getTotalAllEvents(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/publish", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await eventsModel.getAllPublishEvents();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/data/:id_event", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await eventsModel.getFirstEvent(req.params.id_event);
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/attendance/:id_event", async function (req, res) {
  let result = { status: 401 };
  let auth = AUTH.authUserToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      result.data = await eventsModel.getEventAttendanceByUser(
        auth.user.id_user,
        req.params.id_event
      );
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.get("/attendance/event/:id_event", async function (req, res) {
  let result = { status: 401 };
  let auth = AUTH.authAdminToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      result.data = await eventsModel.getEventAttendanceByEvent(
        req.params.id_event
      );
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.post(
  "/",
  [
    body("event_name").notEmpty().trim().escape(),
    body("body").notEmpty().trim().escape(),
    body("starting_date")
      .matches(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)
      .trim()
      .escape(),
    body("author")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .trim()
      .escape(),
    body("location").notEmpty().trim().escape(),
    body("admission").notEmpty().trim().escape(),
    body("image").custom((_, { req, res, next }) => {
      if (!req.files || !req.files.image || req.files.image.length === 0) {
        throw new Error("image is required");
      }

      if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
        throw new Error("image must be type of an image");
      }
      const fileSize = req.files.image.size / 1024 / 1024; // in MB
      if (fileSize > 4) {
        throw new Error("image must be less than 4MB");
      }
      return true;
    }),
    body("isPartner").isBoolean(),
  ],
  async function (req, res) {
    console.log(req.body);
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          try {
            const pathToUpload = __dirname + "/../uploads/events";
            const { fileName } = await upload({
              type: "image",
              file: req.files.image,
              path: pathToUpload,
              bucket: "mitrapabrik/events",
            });
            req.body.image = fileName;
            result.data = await eventsModel.addEvent(req.body);
            result.status = 200;
          } catch (error) {
            result.errors.push(error);
          }
        }
      } catch (error) {
        result.errors.push(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/",
  [
    body("id_event").isNumeric().trim().escape(),
    body("event_name").notEmpty().trim().escape(),
    body("body").notEmpty().trim().escape(),
    body("starting_date")
      .matches(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)
      .trim()
      .escape(),
    body("author")
      .matches(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)
      .trim()
      .escape(),
    body("location").notEmpty().trim().escape(),
    body("admission").notEmpty().trim().escape(),
    body("image").custom((_, { req, res, next }) => {
      if (!req.files || !req.files.image || req.files.image.length === 0) {
        throw new Error("image is required");
      }

      if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
        throw new Error("image must be an image");
      }
      const fileSize = req.files.image.size / 1024 / 1024; // in MB
      if (fileSize > 2) {
        throw new Error("image must be less than 2MB");
      }
      return true;
    }),
    body("isPartner").isBoolean(),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          let id_event = req.body.id_event;
          delete req.body["id_event"];
          const pathToUpload = __dirname + "/../uploads/events";
          const { fileName } = await upload({
            type: "image",
            file: req.files.image,
            path: pathToUpload,
            bucket: "mitrapabrik/events",
          });
          req.body.image = fileName;
          result.data = await eventsModel.editEvent(id_event, req.body);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/publish",
  [body("id_event").isNumeric(), body("status").isIn(["0", "1"])],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));

    if (auth.status != 200) {
      return res.status(auth.status).send(auth);
    }

    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(401).json({ errors: errors.array() });
      } else {
        let checkData = await eventsModel.getFirstEvent(req.body.id_event);
        if (checkData) {
          result.data = await eventsModel.editEvent(req.body.id_event, {
            status: req.body.status,
          });
          if (req.body.status == 1) {
            const link = `/home/event/${checkData.id_event}`;
            const message = `New Event: <b>${checkData.event_name}</b>`;
            const check = await noticeModel.checkLink(link);
            if (!check) {
              await noticeModel.pushNotice({
                message: message,
                link: link,
              });
            }
          }
          result.status = 200;
        } else {
          result.status = 400;
          result.msg = "event tidak ditemukan";
        }
      }
    } catch (error) {
      console.log(error);
    }

    res.status(result.status).send(result);
  }
);

router.delete(
  "/",
  [body("id_event").isNumeric().trim().escape()],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          result.data = await eventsModel.deleteEvent(req.body.id_event);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.post("/register/:id_event", async function (req, res) {
  let result = { status: 401, errors: [] };
  let auth = AUTH.authUserToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
    res.status(result.status).send(result);
  } else {
    try {
      let checkRegister = await eventsModel.getEventAttendanceByUser(
        auth.user.id_user,
        req.params.id_event
      );

      if (!checkRegister) {
        let registerData = {
          id_trans: uuidv1(),
          id_event: req.params.id_event,
          id_user: auth.user.id_user,
          attendance: 0,
          timestamp: plugins.formatDate(new Date()),
        };

        const registerEvent = await eventsModel.registerEvent(registerData).then(async (res) => {

          let eventDetails = await eventsModel.getEventDataByUser(
            auth.user.id_user,
            req.params.id_event
          );

          readHTMLFile(
            path.join(__dirname, "../", "asset", "event-invoice.html"),
            async function (err, html) {
              let template = handlebars.compile(html);
              QRCode.toString("haloo johan", {}, async function (err, url) {
                let replacements = {
                  QR: `https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${eventDetails.id_trans}`,
                  event_name: eventDetails.event_name,
                  starting_date: eventDetails.starting_date,
                  author: eventDetails.author,
                  location: eventDetails.location,
                  admission: eventDetails.admission,
                  nama_depan: auth.user.nama_depan,
                };
                let htmlToSend = template(replacements);
                let sendEmail = await plugins.handleMailerAPI(
                  "https://api.elasticemail.com/v4/emails/transactional",
                  `Invoice Registrasi Event Mitrapabrik`,
                  htmlToSend,
                  auth.user.email
                );
              });
            }
          );

        });

        result.data = registerEvent;
        result.status = 200;
        res.status(result.status).send(result);


      } else {
        result.msg = "anda telah mendaftar pada event ini.";
        res.status(result.status).send(result);
      }
    } catch (error) {
      console.log(error);
      result.msg =
        "mohon maaf, terjadi kesalahan. Silahakan coba beberapa saat lagi.";
      res.status(result.status).send(result);
    }
  }
});

router.put("/attend/:id_trans", async function (req, res) {
  let result = { status: 401, errors: [] };
  let auth = AUTH.authAdminToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      result.data = await eventsModel.confirmAttendance(req.params.id_trans);
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.put("/:id_event/tipe", async function (req, res) {
  let result = { status: 401, errors: [] };
  let auth = AUTH.authAdminToken(req.header("auth-token"));
  if (auth.status != 200) {
    result = auth;
  } else {
    try {
      result.data = await eventsModel.editEvent(req.params.id_event, req.body);
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.get("/generateQr", roles(["User"]), controller.generateQR);

module.exports = router;
