require('dotenv').config();
const express = require("express");
const router = express.Router();
const { body, validationResult } = require('express-validator');
const AUTH = require('../auth')
const requestsModel = require('../models/requests')

router.get('/', async function(req, res) {
    let result = {status: 401, errors:[]}
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if(auth.status != 200) {
        result = auth;
    }
    else {
        try {
            result.data = await requestsModel.getAllrequests()
            result.status = 200
        } catch (error) {
            console.log(error)   
        }
    }
    res.status(result.status).send(result)
})

router.get('/:id_user', async function(req, res) {
    let result = {status: 401, errors:[]}
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if(auth.status != 200) {
        result = auth;
    }
    else {
        try {
            result.data = await requestsModel.getRequestByUser(req.params.id_user)
            result.status = 200
        } catch (error) {
            console.log(error)   
        }
    }
    res.status(result.status).send(result)
})

router.post('/',[
    body('kebutuhan').isLength({ min: 3, max:254 }).trim().escape(),
    body('lokasi').isLength({ min: 3, max:254 }).trim().escape(),
    body('brand').isLength({ min: 3, max:254 }).trim().escape(),
    body('material_qty').isLength({ min: 3, max:254 }).trim().escape(),
],async function(req, res) {
    let result = {status: 401, errors:[]}
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if(auth.status != 200) {
        result = auth;
    }
    else {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(401).json({ errors: errors.array() });
            }
            else {
                req.body.id_user = auth.user.id_user
                result.data = await requestsModel.addNewRequest(req.body)
                result.status = 200
            }
        } catch (error) {
            console.log(error)   
        }
    }
    res.status(result.status).send(result)
})

router.put('/finish/:id_request',async function(req, res) {
    let result = {status: 401, errors:[]}
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if(auth.status != 200) {
        result = auth;
    }
    else {
        try {
            result.data = await requestsModel.finishRequest(req.params.id_request)
            result.status = 200
        } catch (error) {
            console.log(error)  
        }
    }
    res.status(result.status).send(result)
})

module.exports = router;