require('dotenv').config();
const express = require("express");
const router = express.Router();
const { body, validationResult } = require('express-validator');
const AUTH = require('../auth')
const noticeModel = require('../models/notice')
const { randomString } = require("../helpers/randomString");
const axios = require('axios');

router.get('/', async function (req, res) {
    let result = { status: 401, errors: [] }
    try {
        let notices = await noticeModel.getAllNotice();

        notices.forEach(async (notice) => {
            notice.id = randomString(15);

            let paramsNotice = {
                message: notice.message,
                link: notice.link,
                createat: notice.createat,
                broadcast: notice.broadcast,
                id: notice.id
            };
    
            postRequest = await axios({
                method: 'post',
                url: 'http://127.0.0.1:8090/api/collections/notice/records',
                headers: {
                    'accept': "application/json",
                    'content-type': "application/json",
                },
                data: paramsNotice
            }).then((res) => {
                console.log(notice)
                noticeModel.update(notice.id_notice, { publish: 1 });
            });
    
        });

        result.data = notices;
        result.status = 200
    } catch (error) {
        console.log(error)
    }
    res.status(result.status).send(result)
});

router.put('/:id', async function (req, res) {
    let result = { status: 401, errors: [] }

    try {
        const data = {
            publish: 1
        }
        result.data = await noticeModel.update(req.params.id, data)
        result.status = 200
    } catch (error) {
        console.log(error)
    }

    res.status(result.status).send(result)
})



router.post('/', [
    body('message').isLength({ min: 3, max: 254 }).trim().escape(),
    body('link').isLength({ min: 3, max: 254 }).trim().escape()

], async function (req, res) {
    let result = { status: 401, errors: [] }
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if (auth.status != 200) {
        result = auth;
    }
    else {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(401).json({ errors: errors.array() });
            }
            else {
                result.data = await noticeModel.pushNotice(req.body)
                result.status = 200
            }
        } catch (error) {
            console.log(error)
        }
    }
    res.status(result.status).send(result)
})

router.delete('/', [
    body('id_notice').isNumeric().trim().escape(),
], async function (req, res) {
    let result = { status: 401, errors: [] }
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
        result = auth;
    }
    else {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(401).json({ errors: errors.array() });
            }
            else {
                result.data = await noticeModel.deleteNotice(req.body.id_notice)
                result.status = 200
            }
        } catch (error) {
            console.log(error)
        }
    }
    res.status(result.status).send(result)
})

router.get('/all', async function (req, res) {
    let result = { status: 401, errors: [] }
    try {

        let request = await axios({
            method: 'get',
            url: 'http://127.0.0.1:8090/api/collections/notice/records?perPage=100',
            headers: {
                'accept': "application/json",
                'content-type': "application/json",
            }
        });

        let items = request.data.items;
        let countNotice = items.length;

        let requestUsers = await axios({
            method: 'get',
            url: 'http://127.0.0.1:8090/api/collections/count_notice/records?perPage=500',
            headers: {
                'accept': "application/json",
                'content-type': "application/json",
            }
        });

        let itemsUsers = requestUsers.data.items;

        // if (items.length > 0) {

        //     items.forEach(async (noti) => {
        //         if (itemsUsers.length > 0) {
        //             itemsUsers.forEach(async (user) => {
                       
        //                 const userId = user.userid;
        //                 const count = user.count;
        //                 const total = countNotice + count;
    
        //                 const params = {
        //                     noticeid: noti.id,
        //                     userid: userId,
        //                     read: false
        //                 };

        //                 reqNotice = await axios({
        //                     method: 'post',
        //                     url: 'http://127.0.0.1:8090/api/collections/notice_user/records',
        //                     headers: {
        //                         'accept': "application/json",
        //                         'content-type': "application/json",
        //                     },
        //                     data: params
        //                 });

        //                 const paramsUser = {
        //                     count: total
        //                 };

        //                 reqUpdate = await axios({
        //                     method: 'PATCH',
        //                     url: 'http://127.0.0.1:8090/api/collections/count_notice/records/' + user.id,
        //                     headers: {
        //                         'accept': "application/json",
        //                         'content-type': "application/json",
        //                     },
        //                     data: paramsUser
        //                 });
    
        //             });
        //         }

        //         const paramsUser = {
        //             broadcast: true
        //         };

        //         reqUpdate = await axios({
        //             method: 'PATCH',
        //             url: 'http://127.0.0.1:8090/api/collections/notice/records/' + noti.id,
        //             headers: {
        //                 'accept': "application/json",
        //                 'content-type': "application/json",
        //             },
        //             data: paramsUser
        //         });
        //     });
        

        // }

        result.data = itemsUsers;
        result.status = 200
    } catch (error) {
        console.error(error)
    }
    res.status(result.status).send(result)
});

module.exports = router;