const express = require("express");
const router = express.Router();
const controller = require("../controllers/project");
const validations = require("../validations/project");
const projectsModel = require("../models/project");
const { roles } = require("../middlewares/authorizations/roleBased");
const AUTH = require("../auth");

router.post("/", [validations.add], controller.add);

router.get("/", [validations.list, roles(["Prinsipal"])], controller.list);
//router.get("/:id", [validations.list, roles(["Prinsipal"])], controller.list);

router.get("/:id", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await projectsModel.detailProject(req.params.id);
    result.status = 200;
  } catch (error) {
    console.log(error);
  }

  res.status(result.status).send(result);
});

router.get("/admin/list", async function (req, res) {
  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await projectsModel.getAdminProjects(search, orderBy, orderMethod, perPage, page);
    const totalRows = await projectsModel.getTotalProjects(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }

  res.status(result.status).send(result);
});

router.get("/user/list", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authUserToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await projectsModel.getUserProjects(auth.user.id_user, search, orderBy, orderMethod, perPage, page);
    const totalRows = await projectsModel.getTotalUserProjects(auth.user.id_user, search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }

  res.status(result.status).send(result);
});

router.post(
  "/reply",
  [validations.reply, roles(["Prinsipal"])],
  controller.reply
);

router.put(
  "/:id",
  [validations.updateStatus, roles(["User"])],
  controller.updateStatus
);

router.delete(
  "/",
  [validations.deleteProject],
  controller.deleteProject
);

module.exports = router;
