require("dotenv").config();
const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
const AUTH = require("../auth");
const artikelsModel = require("../models/artikels");
const noticeModel = require("../models/notice");
const fs = require("fs");
const path = require("path");
// let Jimp = require('jimp');
const sharp = require("sharp");
const AWS = require("aws-sdk");
const { v1: uuidv1 } = require("uuid");
const { upload } = require("../helpers/uploader");

const IMAGE_MIME_TYPES = [
  "image/png",
  "image/jpg",
  "image/jpeg",
  "image/gif",
  "image/webp",
];

const spacesEndpoint = new AWS.Endpoint("sgp1.digitaloceanspaces.com");
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.DO_SPACES_KEY,
  secretAccessKey: process.env.DO_SPACES_SECRET,
});

function removeTempFile(file_dir) {
  let files = fs.readdirSync(path.join(__dirname, "../", "uploads", file_dir));
  files.forEach((file) => {
    if (file.length < 10) {
      fs.unlinkSync(path.join(__dirname, "../", "uploads", file_dir, file));
    }
  });
}

router.get("/", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const filterType = req.query.tipe;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};
  
  if(auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await artikelsModel.getAllArtikels(filterType, search, orderBy, orderMethod, perPage, page);
    const totalRows = await artikelsModel.getTotalArtikels(filterType, search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/latest", async function (req, res) {
  
  let result = {};
  
  try {
    result.data = await artikelsModel.getLatestArticle();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/publish", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await artikelsModel.getAllPublishArtikels();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/artikel/:id_artikel", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await artikelsModel.getArtikelById(req.params.id_artikel);
    console.log(result.data);
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.post(
  "/",
  [
    body("title")
      .trim()
      .escape(),
    body("body").notEmpty().trim().escape(),
    body("category").notEmpty().trim().escape(),
    body("author").notEmpty().trim().escape(),
    body("tipe").isIn(["ARTIKEL", "FAQ"]),
    body("image").custom((_, { req, res, next }) => {
      if (!req.files || !req.files.image || req.files.image.length === 0) {
        throw new Error("image is required");
      }

      if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
        throw new Error("image must be type of an image");
      }

      return true;
    }),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          console.log('ee')
          return res.status(401).json({ errors: errors.array() });
        } else {

          console.log('ee ok')

          try {
            const pathToUpload = __dirname + "/../uploads/articles";
            const { fileName } = await upload({
              type: "image",
              file: req.files.image,
              path: pathToUpload,
              bucket: "mitrapabrik/articles",
            });
            req.body.image = fileName;
            let data = {
              title: req.body.title,
              body: req.body.body,
              category: req.body.category,
              author: req.body.author,
              tipe: req.body.tipe,
              image: req.body.image,
            };
            result.data = await artikelsModel.addArtikel(data);
            result.status = 200;
          } catch (error) {
            result.errors.push(error);
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/",
  [
    body("id_artikel").isNumeric().trim().escape(),
    body("title")
      .trim()
      .escape(),
    body("body").notEmpty().trim().escape(),
    body("category").notEmpty().trim().escape(),
    body("author").notEmpty().trim().escape(),
    body("timestamp").notEmpty().trim().escape(),
    body("image").custom((_, { req, res, next }) => {
      if (!req.files || !req.files.image || req.files.image.length === 0) {
        throw new Error("image is required");
      }

      if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
        throw new Error("image must be an image");
      }

      return true;
    }),
    body("tipe").isIn(["ARTIKEL", "FAQ"]),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          let id_artikel = req.body.id_artikel;
          delete req.body["id_artikel"];
          const pathToUpload = __dirname + "/../uploads/articles";
          const { fileName } = await upload({
            type: "image",
            file: req.files.image,
            path: pathToUpload,
            bucket: "mitrapabrik/articles",
          });
          req.body.image = fileName;
          result.data = await artikelsModel.editArtikel(id_artikel, req.body);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/publish",
  [body("id_artikel").isNumeric(), body("status").isIn(["0", "1"])],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          let checkData = await artikelsModel.getArtikelById(
            req.body.id_artikel
          );
          if (checkData.length > 0) {
            result.data = await artikelsModel.editArtikel(req.body.id_artikel, {
              status: req.body.status,
            });
            if (req.body.status == 1) {
              const link =  `/home/artikel/${checkData[0].id_artikel}`;
              const message = `New Article: <b>${checkData[0].title}</b>`;
              const check = await noticeModel.checkLink(link);
              if(!check){
                await noticeModel.pushNotice({
                  message: message,
                  link: link,
                });
              }
            }
            result.status = 200;
          } else {
            result.msg = "artikel tidak ditemukan.";
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.delete(
  "/",
  [body("id_artikel").isNumeric().trim().escape()],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          result.data = await artikelsModel.deleteArtikel(req.body.id_artikel);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.get("/asset", async function (req, res) {
  let result = { status: 401, data: [], errors: [] };
  try {
    let files = fs.readdirSync(path.join(__dirname, "../", "uploads", "asset"));
    files.forEach((file) => {
      result.data.push(file);
    });
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.post("/asset/upload", async function (req, res) {
  let result = { status: 401 };
  let token = req.header("auth-token");
  let auth = AUTH.authAdminToken(token);
  if (auth.status != 200) result = auth;
  else {
    try {
      if (req.files.gambar) {
        if (req.files.gambar instanceof Array) {
          req.files.gambar.forEach(async (foto, idx) => {
            let extension = foto.mimetype.split("/")[1];
            let mimeValidation = false,
              dir = "",
              key = uuidv1().toString();
            let newName = `${key}.${extension}`;
            if (IMAGE_MIME_TYPES.includes(foto.mimetype)) {
              mimeValidation = true;
              dir = "asset";
            }
            if (mimeValidation) {
              await foto.mv(
                path.join(__dirname, "../", "uploads", dir, newName)
              );
              let compressResult = await sharp(
                path.join(__dirname, "../", "uploads", dir, newName)
              )
                .webp({ quality: 80 })
                .toBuffer();
              fs.writeFileSync(
                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                compressResult
              );

              await fs.readFile(
                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                (err, finalFile) => {
                  var params = {
                    Bucket: `mitrapabrik/${dir}`,
                    Key: key,
                    Body: finalFile,
                    ACL: "public-read",
                    ContentType: "image/webp",
                    ContentEncoding: "base64",
                  };
                  s3.putObject(params, function (err, data) {
                    if (err) {
                      console.log(err, err.stack);
                    } else {
                      console.log(data);
                    }
                  });
                }
              );
              removeTempFile(dir);
            } else {
              console.log("mime invalid");
            }
          });
        } else {
          let foto = req.files.gambar;
          let extension = foto.mimetype.split("/")[1];
          let mimeValidation = false,
            dir = "",
            key = uuidv1().toString();
          let newName = `${0}.${extension}`;
          if (IMAGE_MIME_TYPES.includes(foto.mimetype)) {
            mimeValidation = true;
            dir = "asset";
          }
          if (mimeValidation) {
            await foto.mv(path.join(__dirname, "../", "uploads", dir, newName));
            let compressResult = await sharp(
              path.join(__dirname, "../", "uploads", dir, newName)
            )
              .webp({ quality: 80 })
              .toBuffer();
            fs.writeFileSync(
              path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
              compressResult
            );

            await fs.readFile(
              path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
              (err, finalFile) => {
                var params = {
                  Bucket: `mitrapabrik/${dir}`,
                  Key: key,
                  Body: finalFile,
                  ACL: "public-read",
                  ContentType: "image/webp",
                  ContentEncoding: "base64",
                };
                s3.putObject(params, function (err, data) {
                  if (err) {
                    console.log(err, err.stack);
                  } else {
                    console.log(data);
                  }
                });
              }
            );
            removeTempFile(dir);
          } else {
            console.log("mime invalid");
          }
        }
        result.status = 200;
      }
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

router.delete("/asset/:id_asset", async function (req, res) {
  let result = { status: 401, data: [], errors: [] };
  let token = req.header("auth-token");
  let auth = AUTH.authAdminToken(token);
  if (auth.status != 200) result = auth;
  else {
    try {
      let files = fs.readdirSync(
        path.join(__dirname, "../", "uploads", "asset")
      );
      files.forEach((file) => {
        if (file == req.params.id_asset) {
          fs.unlinkSync(path.join(__dirname, "../", "uploads", "asset", file));
          console.log(`file ${file} deleted`);
        }
      });
      result.status = 200;
    } catch (error) {
      console.log(error);
    }
  }
  res.status(result.status).send(result);
});

module.exports = router;
