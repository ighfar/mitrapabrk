require('dotenv').config();
const express = require("express");
const router = express.Router();
const fs = require("fs");
const { body, validationResult } = require('express-validator');
const AUTH = require('../auth');
const showroomModel = require('../models/showrooms');
const noticeModel = require('../models/notice');
const AWS = require("aws-sdk");
const { upload } = require("../helpers/uploader");
const sharp = require("sharp");
const path = require("path");

const IMAGE_MIME_TYPES = [
    "image/png",
    "image/jpg",
    "image/jpeg",
    "image/gif",
    "image/webp",
];

const spacesEndpoint = new AWS.Endpoint("sgp1.digitaloceanspaces.com");
const s3 = new AWS.S3({
    endpoint: spacesEndpoint,
    accessKeyId: process.env.DO_SPACES_KEY,
    secretAccessKey: process.env.DO_SPACES_SECRET,
});

function removeDuplicateFile(file_dir, filename) {
    let files = fs.readdirSync(path.join(__dirname, "../", "uploads", file_dir));
    files.forEach((file) => {
        if (file.includes(`${filename}.`)) {
            fs.unlinkSync(path.join(__dirname, "../", "uploads", file_dir, file));
        }
    });
}

router.get('/', async function (req, res) {
    let result = { status: 401, errors: [], data: {} }
    try {
        result.data = await showroomModel.getAllShowroom()
        result.status = 200
    } catch (error) {
        console.log(error)
    }
    res.status(result.status).send(result)
});

router.get('/publish', async function (req, res) {
    let result = { status: 500, errors: [], data: {} }
    try {
        const showroomCity = await showroomModel.getAllShowroomByCity();
        const tempShowrooms = [];

        await Promise.all(showroomCity.map(async (item) => {
            item.hub = await showroomModel.findByCity(item.kota);
            tempShowrooms.push(item)
        }));
        result.data = tempShowrooms;
        result.status = 200;
    } catch (error) {
        console.log(error)
    }
    res.status(result.status).send(result)
});

router.get('/find/:id', async function (req, res) {
    const id = req.params.id;
    let result = { status: 500, errors: [], data: {} }
    try {


        // await Promise.all(async () => {
            const showroom = await showroomModel.getShowroomByIdFirst(id);
            const listBrand  = showroom.list_brand;
            showroom.principal = await showroomModel.getPrincipal(listBrand.split(","));
        // });

        result.data = showroom;
        result.status = 200;
    } catch (error) {
        console.log(error)
    }
    res.status(result.status).send(result)
});

router.get('/list', async function (req, res) {
    const perPage = req.query.perPage;
    const page = req.query.page;
    const search = req.query.search;
    const orderBy = req.query.orderBy;
    const orderMethod = req.query.orderMethod;

    let auth = AUTH.authAdminToken(req.header("auth-token"));

    let result = {};

    if (auth.status != 200) {
        return res.status(auth.status).send(auth);
    }

    try {
        result.data = await showroomModel.getAllShowroomList(search, orderBy, orderMethod, perPage, page);
        const totalRows = await showroomModel.getTotalShowrooms(search);
        result.data.totalRows = totalRows[0].total;
        result.status = 200
    } catch (error) {
        console.log(error)
    }
    res.status(result.status).send(result)
})

router.post('/add', [
    body('title').isLength({ min: 3, max: 254 }).trim().escape(),
    body('alamat').isLength({ min: 3, max: 254 }).trim().escape(),
    body('list_brand'),
    body('detail_alamat').notEmpty(),
    body('deskripsi').notEmpty(),
    body('kota').isLength({ min: 3, max: 254 }).trim().escape(),
    body('drop_pin').isLength({ min: 3, max: 254 }).trim().escape(),
    body('wa_contact').isLength({ min: 3, max: 254 }).trim().escape(),
    body('status').isIn(['1', '0']),
    body("image").custom((_, { req, res, next }) => {
        if (!req.files || !req.files.image || req.files.image.length === 0) {
            throw new Error("image is required");
        }

        if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
            throw new Error("image must be type of an image");
        }
        const fileSize = req.files.image.size / 1024 / 1024; // in MB
        if (fileSize > 4) {
            throw new Error("image must be less than 4MB");
        }
        return true;
    }),
], async function (req, res) {
    let result = { status: 401, errors: [] }
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
        result = auth;
    }
    else {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(401).json({ errors: errors.array() });
            }
            else {
                const pathToUpload = __dirname + "/../uploads/showrooms";
                const { fileName } = await upload({
                    type: "image",
                    file: req.files.image,
                    path: pathToUpload,
                    bucket: "mitrapabrik/showrooms",
                });

                let tmpGallery = [];
                let galleryText = '';
                if (req.files.galery.length > 0) {
                    req.files.galery.forEach(async (foto, idx) => {

                        let extension = foto.mimetype.split("/")[1];
                        let newName = `${foto.md5}.${extension}`;
                        let mimeValidation = false,
                            dir = "",
                            key;


                        if (IMAGE_MIME_TYPES.includes(foto.mimetype)) {
                            mimeValidation = true;
                            dir = "showrooms";
                            key = foto.md5;
                            tmpGallery.push(`${key}`);
                        }
                        if (mimeValidation) {
                            foto.mv(`${pathToUpload}/${newName}`);
                            let compressResult = await sharp(
                                `${pathToUpload}/${newName}`
                            )
                                .webp({ quality: 80 })
                                .toBuffer();
                            fs.writeFileSync(
                                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                                compressResult
                            );

                            fs.readFile(
                                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                                (err, finalFile) => {
                                    var params = {
                                        Bucket: `mitrapabrik/${dir}`,
                                        Key: key,
                                        Body: finalFile,
                                        ACL: "public-read",
                                        ContentType: "image/webp",
                                        ContentEncoding: "base64",
                                    };
                                    s3.putObject(params, function (err, data) {
                                        if (err) {
                                            console.log(err, err.stack);
                                        } else {
                                            console.log(data);
                                        }
                                    });
                                }
                            );

                            fs.unlinkSync(`${pathToUpload}/${newName}`);
                            fs.unlinkSync(`${pathToUpload}/${key}.webp`);

                        } else {
                            console.log("mime invalid");
                        }

                    });
                }

                galleryText = tmpGallery.toString();
                req.body.image = fileName;
                req.body.galery = galleryText;

                console.log(req.body);

                result.data = await showroomModel.addNewShowroom(req.body)
                result.status = 200
            }


        } catch (error) {
            result.status = 500
            result.errors = error
        }
    }
    res.status(result.status).send(result)
})

router.put('/update', [
    body('id_showroom').isNumeric().trim().escape(),
    body('title').isLength({ min: 3, max: 254 }).trim().escape(),
    body('alamat').isLength({ min: 3, max: 254 }).trim().escape(),
    body('list_brand'),
    body('detail_alamat').notEmpty(),
    body('deskripsi').notEmpty(),
    body('kota').isLength({ min: 3, max: 254 }).trim().escape(),
    body('drop_pin').isLength({ min: 3, max: 254 }).trim().escape(),
    body('wa_contact').isLength({ min: 3, max: 254 }).trim().escape(),
    body('status').isInt().trim().escape(),
    body("image").custom((_, { req, res, next }) => {
        if (!req.files || !req.files.image || req.files.image.length === 0) {
            throw new Error("image is required");
        }

        if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
            throw new Error("image must be type of an image");
        }
        const fileSize = req.files.image.size / 1024 / 1024; // in MB
        if (fileSize > 4) {
            throw new Error("image must be less than 4MB");
        }
        return true;
    }),
], async function (req, res) {
    let result = { status: 401, errors: [] }
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
        result = auth;
    }
    else {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(401).json({ errors: errors.array() });
            }
            else {
                const pathToUpload = __dirname + "/../uploads/showrooms";
                const { fileName } = await upload({
                    type: "image",
                    file: req.files.image,
                    path: pathToUpload,
                    bucket: "mitrapabrik/showrooms",
                });

                let tmpGallery = [];
                let galleryText = '';
                if (req.files.galery.length > 0) {
                    req.files.galery.forEach(async (foto, idx) => {
                        let extension = foto.mimetype.split("/")[1];
                        let newName = `${foto.md5}.${extension}`;
                        let mimeValidation = false,
                            dir = "",
                            key;
                        if (IMAGE_MIME_TYPES.includes(foto.mimetype)) {
                            mimeValidation = true;
                            dir = "showrooms";
                            key = foto.md5;
                            tmpGallery.push(`${key}`);
                        }
                        if (mimeValidation) {
                            await foto.mv(
                                path.join(__dirname, "../", "uploads", dir, newName)
                            );
                            let compressResult = await sharp(
                                path.join(__dirname, "../", "uploads", dir, newName)
                            )
                                .webp({ quality: 80 })
                                .toBuffer();
                            fs.writeFileSync(
                                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                                compressResult
                            );

                            fs.readFile(
                                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                                (err, finalFile) => {
                                    var params = {
                                        Bucket: `mitrapabrik/${dir}`,
                                        Key: key,
                                        Body: finalFile,
                                        ACL: "public-read",
                                        ContentType: "image/webp",
                                        ContentEncoding: "base64",
                                    };
                                    s3.putObject(params, function (err, data) {
                                        if (err) {
                                            console.log(err, err.stack);
                                        } else {
                                            console.log(data);
                                        }
                                    });
                                }
                            );

                            fs.unlinkSync(`${pathToUpload}/${newName}`);
                            fs.unlinkSync(`${pathToUpload}/${key}.webp`);

                        } else {
                            console.log("mime invalid");
                        }

                    });
                }
                galleryText = tmpGallery.toString();
                req.body.image = fileName;
                req.body.galery = galleryText;
                let id_showroom = req.body.id_showroom
                delete req.body["id_showroom"]

                console.log(req.body);

                result.data = await showroomModel.updateShowroom(id_showroom, req.body)
                result.status = 200
            }
        } catch (error) {
            console.log(error)
        }
    }
    res.status(result.status).send(result)
})

router.put('/publish', [
    body('id_showroom').isNumeric(),
    body('status').isIn(['0', '1']),
], async function (req, res) {
    let result = { status: 401, errors: [] }
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
        result = auth;
    }
    else {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(401).json({ errors: errors.array() });
            }
            else {
                let checkData = await showroomModel.getShowroomById(req.body.id_showroom)
                if (checkData.length > 0) {
                    result.data = await showroomModel.updateShowroom(req.body.id_showroom, { status: req.body.status })
                    if (req.body.status == 1) {

                        const link = `/home/showroom/`;
                        const message = `New Showroom: <b>${checkData[0].title} (${checkData[0].kota})</b>`;
                        const check = await noticeModel.checkMessage(message);

                        if (!check) {
                            await noticeModel.pushNotice({
                                message: message,
                                link: link,
                            });
                        }
                    }

                    result.status = 200
                } else {
                    result.msg = "showroom tidak ditemukan"
                }
            }
        } catch (error) {
            console.log(error)
        }
    }
    res.status(result.status).send(result)
})

router.delete('/delete', [
    body('id_showroom').isNumeric().trim().escape(),
], async function (req, res) {
    let result = { status: 401, errors: [] }
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
        result = auth;
    }
    else {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(401).json({ errors: errors.array() });
            }
            else {
                result.data = await showroomModel.deleteShowroom(req.body.id_showroom)
                result.status = 200
            }
        } catch (error) {
            console.log(error)
        }
    }
    res.status(result.status).send(result)
})

module.exports = router;