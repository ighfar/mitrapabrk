const express = require("express");
const router = express.Router();
const controller = require("../controllers/reply");
const validations = require("../validations/reply");
const { roles } = require("../middlewares/authorizations/roleBased");

router.use(roles(["Prinsipal"]));

router.post("/", validations.add, controller.add);

router.get("/:id", validations.get, controller.get);

router.put("/:id", validations.update, controller.update);

module.exports = router;
