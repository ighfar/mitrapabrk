require('dotenv').config();
const express = require("express");
const router = express.Router();

const AUTH = require('../auth')
const userModel = require('../models/user')

router.get('/', async function(req, res) {
    let result = {status: 401, errors:[]}
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if(auth.status != 200) {
        result = auth;
    }
    else {
        try {
            result.data = await userModel.getUserWishlistByUser(auth.user.id_user)
            result.status = 200
        } catch (error) {
            console.log(error)   
        }
    }
    res.status(result.status).send(result)
})

router.post('/:id_product', async function(req, res) {
    let result = {status: 401, errors:[]}
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if(auth.status != 200) {
        result = auth;
    }
    else {
        try {
            let checkData = await userModel.getUserWishlistByItem(auth.user.id_user, req.params.id_product)
            if(checkData.length > 0) {
                result.msg = "Produk sudah terdapat di wishlist"
                result.status = 200
            } else {
                result.data = await userModel.addWishlist(auth.user.id_user, req.params.id_product)
                result.msg = "produk telah disimpan di wishlist"
                result.status = 200
            }
        } catch (error) {
            console.log(error)   
        }
    }
    res.status(result.status).send(result)
})

router.delete('/:id_product', async function(req, res) {
    let result = {status: 401, errors:[]}
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if(auth.status != 200) {
        result = auth;
    }
    else {
        try {
            result.data = await userModel.removeWishlist(auth.user.id_user, req.params.id_product)
            result.status = 200
        } catch (error) {
            console.log(error)   
        }
    }
    res.status(result.status).send(result)
})

module.exports = router;