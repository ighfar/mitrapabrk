require("dotenv").config();
const express = require("express");
const router = express.Router();
const { body, validationResult, param } = require("express-validator");
const AUTH = require("../auth");
const fs = require("fs");
const path = require("path");
const sharp = require("sharp");
const AWS = require("aws-sdk");
const { v1: uuidv1 } = require("uuid");
const md5 = require("crypto-js/md5");
const multer = require("multer");
const productsModel = require("../models/products");
const userModel = require("../models/user");
const searchModel = require("../models/search");
const { upload } = require("../helpers/uploader");
const responseHandler = require("../helpers/responseHandler");

const IMG_MIME_TYPE = ["image/png", "image/jpeg", "image/jpg"];

const spacesEndpoint = new AWS.Endpoint("sgp1.digitaloceanspaces.com");
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.DO_SPACES_KEY,
  secretAccessKey: process.env.DO_SPACES_SECRET,
});

function removeDuplicateFile(file_dir, id_user, id_product, idx) {
  let files = fs.readdirSync(path.join(__dirname, "../", "uploads", file_dir));
  files.forEach((file) => {
    if (file.includes(`${id_user}_${id_product}_${idx}.`)) {
      fs.unlinkSync(path.join(__dirname, "../", "uploads", file_dir, file));
    }
  });
}

function removeByFileName(file_dir, filename) {
  let files = fs.readdirSync(path.join(__dirname, "../", "uploads", file_dir));
  files.forEach((file) => {
    if (file == filename) {
      console.log(
        "removing: " + path.join(__dirname, "../", "uploads", file_dir, file)
      );
      fs.unlinkSync(path.join(__dirname, "../", "uploads", file_dir, file));
    }
  });
}

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads");
  },

  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

router.get("/", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await productsModel.getAllProducts(search, orderBy, orderMethod, perPage, page);
    const totalRows = await productsModel.getTotalProducts(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/list", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;
  const kategoriId = req.query.kategoriId;

  let auth = AUTH.authUserToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {

    const products = await productsModel.getAllProducts(search, orderBy, orderMethod, perPage, page, kategoriId);
    result.data = products;
    const totalRows = await productsModel.getTotalProducts(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/wishlist", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;
  const kategoriId = req.query.kategoriId;

  let auth = AUTH.authUserToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    const userId = auth.user.id_user;
    result.data = await productsModel.getWishlistProducts(search, orderBy, orderMethod, perPage, page, kategoriId, userId);
    const totalRows = await productsModel.getWishlistTotalProducts(search, userId);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/sale/list", async function (req, res) {

  let auth = AUTH.authUserToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await productsModel.getAllProductsSale();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/tipe/new", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    const kategori = await productsModel.getAllKategories();
    const tempKategori = [];

    await Promise.all(kategori.map(async (item) => {
      item.child = await productsModel.getAllChildKategories(item.Id);
      tempKategori.push(item)
    }));

    result.data = kategori;

    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/tipe", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await productsModel.getSubCategories();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/kategori", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await productsModel.getAllRawCategories();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/type/:tipe", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await productsModel.getAllProductsByType(req.params.tipe);
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/user/tipe/:user_id",

  // [body("user_id").isNumeric()], 
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authUserToken(req.header("auth-token"));

    if (auth.status != 200) {
      result = auth;
    } else {
      req.body.user_id = auth.user.id_user
      try {
        result.data = await productsModel.getProductTypeByUser(auth.user.id_user);
        result.status = 200;
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);

  });
router.get("/search", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await searchModel.searchProduct(
      req.query.limit || 12,
      req.query.offset || 0,
      req.query.search || ""
    );
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/tipe/detail", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {
    result.data = await productsModel.getAllProductsByType(req.query.tipe);
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/detail/:id_product", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {

    const productId = req.params.id_product;

    const products = await productsModel.getProductDetail(productId);
    await Promise.all(products.map(async (item) => {
      item.images = await productsModel.getImagesProduct(item.id_produk);
      item.partner = await userModel.getPartnerData(item.id_partner);
    }));

    const otherProducts = await productsModel.getAllProductsOther(productId);
    await Promise.all(otherProducts.map(async (item) => {
      item.images = await productsModel.getImagesProduct(item.id_produk);
    }));

    const [productsdetail, otherProductsDetail, productImages] = await Promise.all([products, otherProducts, productsModel.getImagesProduct(req.params.id_product)]);
    result.data = productsdetail;
    result.other = otherProductsDetail;
    result.images = productImages;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/partner/:id_partner", async function (req, res) {
  let result = { status: 401, errors: [] };
  try {

    const partnerId = req.params.id_partner;

    const products = await productsModel.getAllProductsByPartner(partnerId);
    const tempProducts = [];

    await Promise.all(products.map(async (item) => {
      item.images = await productsModel.getImagesProduct(item.id_produk);
      tempProducts.push(item)
    }));

    result.data = products;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.post(
  "/",
  [
    body("title")
      .matches(/^[a-zA-Z0-9 ]{4,}$/)
      .trim()
      .escape()
      .withMessage("Masukkan nama produk yang valid"),
    body("kategori_id")
      .notEmpty()
      .trim()
      .escape()
      .withMessage("Pilih salah satu kategori"),
    body("varian").notEmpty().withMessage("Masukkan minimal 1 varian").escape(),
    body("deskripsi")
      .notEmpty()
      .trim()
      .escape()
      .withMessage("Deskripsi tidak boleh kosong"),
    //    body("katalog").notEmpty().withMessage("Tambahkan minimal 1 gambar"),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authUserToken(req.header("auth-token"));
    let product_key = "";
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          req.body.id_partner = auth.user.id_user;
          req.body.id_user = auth.user.id_user;
          req.body.katalog = "";
          product_key = await productsModel.addNewProduct(req.body);
          let tmpKatalog = [];
          if (req.files.gambar instanceof Array) {
            req.files.gambar.forEach(async (foto, idx) => {
              let extension = foto.mimetype.split("/")[1];
              let newName = `${auth.user.id_user}_${product_key}_${idx}.${extension}`;
              let mimeValidation = false,
                dir = "",
                key;
              if (IMG_MIME_TYPE.includes(foto.mimetype)) {
                mimeValidation = true;
                dir = "products";
                // key = md5(`${dir}_${auth.user.id_user}_${product_key}_${idx}`).toString()
                key = uuidv1();
                tmpKatalog.push(`${key}`);
              }
              if (mimeValidation) {
                await foto.mv(
                  path.join(__dirname, "../", "uploads", dir, newName)
                );
                let compressResult = await sharp(
                  path.join(__dirname, "../", "uploads", dir, newName)
                )
                  .webp({ quality: 80 })
                  .toBuffer();
                fs.writeFileSync(
                  path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                  compressResult
                );

                await fs.readFile(
                  path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                  (err, finalFile) => {
                    var params = {
                      Bucket: `mitrapabrik/${dir}`,
                      Key: key,
                      Body: finalFile,
                      ACL: "public-read",
                      ContentType: "image/webp",
                      ContentEncoding: "base64",
                    };
                    s3.putObject(params, function (err, data) {
                      if (err) {
                        console.log(err, err.stack);
                      } else {
                        console.log(data);
                      }
                    });
                  }
                );

                const dataProductImage = {
                  product_id: product_key,
                  path: key,
                };
          
                await productsModel.addProductImage(dataProductImage);

                removeDuplicateFile(dir, auth.user.id_user, product_key, idx);
              } else {
                console.log("mime invalid");
              }
            });
          } else {
            let foto = req.files.gambar;
            let extension = foto.mimetype.split("/")[1];
            let newName = `${auth.user.id_user
              }_${product_key}_${0}.${extension}`;
            let mimeValidation = false,
              dir = "",
              key;
            if (IMG_MIME_TYPE.includes(foto.mimetype)) {
              mimeValidation = true;
              dir = "products";
              key = md5(
                `${dir}_${auth.user.id_user}_${product_key}_${0}`
              ).toString();
              tmpKatalog.push(`${key}`);
            }
            if (mimeValidation) {
              await foto.mv(
                path.join(__dirname, "../", "uploads", dir, newName)
              );
              let compressResult = await sharp(
                path.join(__dirname, "../", "uploads", dir, newName)
              )
                .webp({ quality: 80 })
                .toBuffer();
              fs.writeFileSync(
                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                compressResult
              );

              await fs.readFile(
                path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                (err, finalFile) => {
                  var params = {
                    Bucket: `mitrapabrik/${dir}`,
                    Key: key,
                    Body: finalFile,
                    ACL: "public-read",
                    ContentType: "image/webp",
                    ContentEncoding: "base64",
                  };
                  s3.putObject(params, function (err, data) {
                    if (err) {
                      console.log(err, err.stack);
                    } else {
                      console.log(data);
                    }
                  });
                }
              );

              const dataProductImage = {
                product_id: product_key,
                path: key,
              };
        
              await productsModel.addProductImage(dataProductImage);

              removeDuplicateFile(dir, auth.user.id_user, product_key, 0);
            } else {
              console.log("mime invalid");
            }
          }
          await productsModel.editProduct(product_key, auth.user.id_user, {
            katalog: tmpKatalog.toString(),
          });
          result.status = 200;
        }
      } catch (error) {
        // await productsModel.deleteProduct(product_key);
        console.log(product_key);
        console.error(error);
        result.status = 500;
        result.errors = error.message;
        return res.status(result.status).send(result);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/",
  [
    body("title")
      .matches(/^[a-zA-Z0-9 ]{4,}$/)
      .trim()
      .escape()
      .withMessage("Masukkan nama produk yang valid"),
    body("kategori_id")
      .notEmpty()
      .trim()
      .escape()
      .withMessage("Pilih salah satu kategori"),
    body("varian").notEmpty().withMessage("Masukkan minimal 1 varian").escape(),
    body("deskripsi")
      .notEmpty()
      .trim()
      .escape()
      .withMessage("Deskripsi tidak boleh kosong"),
    body("katalog").notEmpty().withMessage("Tambahkan minimal 1 gambar"),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          let id_produk = req.body.id_produk;
          let id_partner = auth.user.id_user;
          let checkData = await productsModel.getProductDetail(id_produk);
          if (checkData.length > 0) {
            let currCatalog =
              (checkData[0].katalog && checkData[0].katalog.split(",")) || [];
            let newKatalog =
              (req.body.katalog && req.body.katalog.split(",")) || [];
            newKatalog = newKatalog.filter((n) => n);
            let removedKatalog = currCatalog.filter(
              (item) => !newKatalog.includes(item)
            );
            let addedItem = newKatalog.filter(
              (itm) => !currCatalog.includes(itm)
            );

            removedKatalog.forEach((k) => {
              removeByFileName("products", k);
            });
            let tmpKatalog = [];
            if (req.files !== null) {
              if (req.files.gambar instanceof Array) {
                req.files.gambar.forEach(async (foto, idx) => {
                  let extension = foto.mimetype.split("/")[1];
                  let newName = `${auth.user.id_user}_${checkData[0].product_key
                    }_${idx + newKatalog.length}.${extension}`;
                  let mimeValidation = false,
                    dir = "",
                    key;
                  if (IMG_MIME_TYPE.includes(foto.mimetype)) {
                    mimeValidation = true;
                    dir = "products";
                    // key = md5(`${dir}_${auth.user.id_user}_${checkData[0].product_key}_${idx + newKatalog.length}`).toString()
                    key = uuidv1();
                    tmpKatalog.push(`${key}`);
                  }
                  if (mimeValidation) {
                    await foto.mv(
                      path.join(__dirname, "../", "uploads", dir, newName)
                    );
                    let compressResult = await sharp(
                      path.join(__dirname, "../", "uploads", dir, newName)
                    )
                      .webp({ quality: 80 })
                      .toBuffer();
                    fs.writeFileSync(
                      path.join(
                        __dirname,
                        "../",
                        "uploads",
                        dir,
                        `${key}.webp`
                      ),
                      compressResult
                    );

                    await fs.readFile(
                      path.join(
                        __dirname,
                        "../",
                        "uploads",
                        dir,
                        `${key}.webp`
                      ),
                      (err, finalFile) => {
                        var params = {
                          Bucket: `mitrapabrik/${dir}`,
                          Key: key,
                          Body: finalFile,
                          ACL: "public-read",
                          ContentType: "image/webp",
                          ContentEncoding: "base64",
                        };
                        s3.putObject(params, function (err, data) {
                          if (err) {
                            console.log(err, err.stack);
                          } else {
                            console.log(data);
                          }
                        });
                      }
                    );

                    removeDuplicateFile(
                      dir,
                      auth.user.id_user,
                      checkData[0].product_key,
                      idx + newKatalog.length
                    );
                  } else {
                    console.log("mime invalid");
                  }
                });
              } else {
                console.log("masuk sini, cuma 1");
                let foto = req.files.gambar;
                let extension = foto.mimetype.split("/")[1];
                let newName = `${auth.user.id_user}_${checkData[0].product_key}_${newKatalog.length}.${extension}`;
                let mimeValidation = false,
                  dir = "",
                  key;
                if (IMG_MIME_TYPE.includes(foto.mimetype)) {
                  mimeValidation = true;
                  dir = "products";
                  // key = md5(`${dir}_${auth.user.id_user}_${checkData[0].product_key}_${newKatalog.length}`).toString()
                  key = uuidv1();
                  tmpKatalog.push(`${key}`);
                }
                if (mimeValidation) {
                  await foto.mv(
                    path.join(__dirname, "../", "uploads", dir, newName)
                  );
                  let compressResult = await sharp(
                    path.join(__dirname, "../", "uploads", dir, newName)
                  )
                    .webp({ quality: 80 })
                    .toBuffer();
                  fs.writeFileSync(
                    path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                    compressResult
                  );

                  await fs.readFile(
                    path.join(__dirname, "../", "uploads", dir, `${key}.webp`),
                    (err, finalFile) => {
                      var params = {
                        Bucket: `mitrapabrik/${dir}`,
                        Key: key,
                        Body: finalFile,
                        ACL: "public-read",
                        ContentType: "image/webp",
                        ContentEncoding: "base64",
                      };
                      s3.putObject(params, function (err, data) {
                        if (err) {
                          console.log(err, err.stack);
                        } else {
                          console.log(data);
                        }
                      });
                    }
                  );

                  removeDuplicateFile(
                    dir,
                    auth.user.id_user,
                    checkData[0].product_key,
                    newKatalog.length
                  );
                } else {
                  console.log("mime invalid");
                }
              }
            }
            newKatalog = newKatalog.filter((itm) => !addedItem.includes(itm));
            req.body.katalog = [...newKatalog, ...tmpKatalog].toString();
            let finalForm = {
              title: req.body.title,
              kategori_id: req.body.kategori_id,
              sale: req.body.sale,
              diskon: req.body.diskon,
              varian: req.body.varian,
              katalog: req.body.katalog,
              deskripsi: req.body.deskripsi,
              status: req.body.status,
            };
            result.data = await productsModel.editProduct(
              id_produk,
              id_partner,
              finalForm
            );
            result.status = 200;
          } else {
            result.msg = "produk tidak ditemukan";
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/status/toggle/:id_produk",
  (body("status").isIn[("0", "1")], param("id_produk").notEmpty().isNumeric()),
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(401).json({ errors: errors.array() });
      } else {
        try {
          let checkData = await productsModel.getProductDetail(
            req.params.id_produk
          );
          if (checkData.length > 0) {
            await productsModel.editProduct(
              req.params.id_produk,
              auth.user.id_user,
              { status: req.body.status == 1 ? 0 : 1 }
            );
            result.status = 200;
            result.msg = "status produk berhasil diubah";
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
    res.status(result.status).send(result);
  }
);

router.post(
  "/delete",
  [body("id_produk").isNumeric().trim().escape()],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authUserToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          let id_produk = req.body.id_produk;
          let id_partner = auth.user.id_user;
          let checkData = await productsModel.getProductDetail(id_produk);
          if (checkData.length > 0) {
            let listKatalog = checkData[0].katalog.split(",");
            listKatalog.forEach((k) => {
              removeByFileName("products", k);
            });
            result.data = await productsModel.deleteProduct(
              id_produk,
              id_partner
            );
            result.status = 200;
          } else {
            result.msg = "id product tidak ditemukan";
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.delete(
  "/delete",
  [body("id_produk").isNumeric().trim().escape()],
  async function (req, res) {

    let auth = AUTH.authAdminToken(req.header("auth-token"));

    let result = {};

    if (auth.status != 200) {
      return res.status(auth.status).send(auth);
    }

    try {

      let id_produk = req.body.id_produk;
      let checkData = await productsModel.getProductById(id_produk);
      if (checkData.length > 0) {
        let listKatalog = checkData[0].katalog.split(",");
        listKatalog.forEach((k) => {
          removeByFileName("products", k);
        });
        result.data = await productsModel.adminDeleteProduct(
          id_produk
        );
        result.status = 200;
      } else {
        result.msg = "id product tidak ditemukan";
      }

    } catch (error) {
      console.log(error);
    }

    res.status(result.status).send(result);
  }
);

router.post(
  "/upload",
  [body("id_produk").isNumeric().trim().escape()],
  async function (req, res) {
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    let result = {};

    if (auth.status != 200) {
      return res.status(auth.status).send(auth);
    }

    try {

      const pathToUpload = __dirname + "/../uploads/products";

      const id_produk = req.body.id_produk;
      const image = req.files.image;

      const respImageUpload = await upload({
        type: "image",
        file: image,
        path: pathToUpload,
        bucket: "mitrapabrik/products",
      });

      const dataProductImage = {
        product_id: id_produk,
        path: respImageUpload.fileName,
      };

      await productsModel.addProductImage(dataProductImage);

      result.status = 200;
      result.message = 'Product image has been added';

    } catch (err) {
      return await responseHandler(err, res);
    }

    return res.status(result.status).send(result);

  }
);

router.post(
  "/image/delete",
  [body("id").isNumeric().trim().escape()],
  async function (req, res) {
    let token = req.header("auth-token");
    let auth = AUTH.authUserToken(token);
    let result = {};

    if (auth.status != 200) {
      return res.status(auth.status).send(auth);
    }

    try {

      const id = req.body.id;

      const productImage = await productsModel.findProductImage(id);

      if(productImage) {
        s3.deleteObject({  
          Bucket: "mitrapabrik/products", 
          Key: productImage.path 
         },async function (err,data){

          if(err == null) {
            await productsModel.deleteProductImage(id);
          }

         });
      }

      result.status = 200;
      result.message = 'Product image has been delete.';

    } catch (err) {
      return await responseHandler(err, res);
    }

    return res.status(result.status).send(result);

  }
);

module.exports = router;
