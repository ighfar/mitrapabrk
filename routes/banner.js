require("dotenv").config();
const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
const AUTH = require("../auth");
const bannerModel = require("../models/banner");
const fs = require("fs");
const { upload } = require("../helpers/uploader");

const IMAGE_MIME_TYPES = [
  "image/png",
  "image/jpg",
  "image/jpeg",
  "image/gif",
  "image/webp",
];

let readHTMLFile = function (path, callback) {
  fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
    if (err) callback(err);
    else callback(null, html);
  });
};

router.get("/", async function (req, res) {

  const perPage = req.query.perPage;
  const page = req.query.page;
  const search = req.query.search;
  const orderBy = req.query.orderBy;
  const orderMethod = req.query.orderMethod;

  let auth = AUTH.authAdminToken(req.header("auth-token"));

  let result = {};

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await bannerModel.getAll(search, orderBy, orderMethod, perPage, page);
    const totalRows = await bannerModel.getTotal(search);
    result.data.totalRows = totalRows[0].total;
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.get("/publish", async function (req, res) {
  let result = {};
  try {
    result.data = await bannerModel.getPublish();
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});


router.get("/:id", async function (req, res) {
  let result = { status: 401, errors: [] };


  let auth = AUTH.authAdminToken(req.header("auth-token"));

  if (auth.status != 200) {
    return res.status(auth.status).send(auth);
  }

  try {
    result.data = await bannerModel.getFirst(req.params.id);
    result.status = 200;
  } catch (error) {
    console.log(error);
  }
  res.status(result.status).send(result);
});

router.post(
  "/",
  [
    body("title").notEmpty().trim(),
    body("image").custom((_, { req, res, next }) => {
      if (!req.files || !req.files.image || req.files.image.length === 0) {
        throw new Error("image is required");
      }

      if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
        throw new Error("image must be type of an image");
      }
      const fileSize = req.files.image.size / 1024 / 1024; // in MB
      if (fileSize > 8) {
        throw new Error("image must be less than 8MB");
      }
      return true;
    }),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          try {
            const pathToUpload = __dirname + "/../uploads/banner";
            const { fileName } = await upload({
              type: "image",
              file: req.files.image,
              path: pathToUpload,
              bucket: "mitrapabrik/banner",
            });
            req.body.image = fileName;
            result.data = await bannerModel.add(req.body);
            result.status = 200;
          } catch (error) {
            result.errors.push(error);
          }
        }
      } catch (error) {
        result.errors.push(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.put(
  "/:id",
  [
    body("title").notEmpty().trim(),
    body("image").custom((_, { req, res, next }) => {

      if (req.files !== null) {
        if (!IMAGE_MIME_TYPES.includes(req.files.image.mimetype)) {
          throw new Error("image must be type of an image");
        }
        const fileSize = req.files.image.size / 1024 / 1024; // in MB
        if (fileSize > 8) {
          throw new Error("image must be less than 8MB");
        }
      }
      return true;
    }),
  ],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          let id = req.params.id;

          if (req.files !== null) {
            const pathToUpload = __dirname + "/../uploads/banner";
            const { fileName } = await upload({
              type: "image",
              file: req.files.image,
              path: pathToUpload,
              bucket: "mitrapabrik/banner",
            });
            req.body.image = fileName;
          }

          result.data = await bannerModel.edit(id, req.body);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

router.delete(
  "/:id",
  [],
  async function (req, res) {
    let result = { status: 401, errors: [] };
    let auth = AUTH.authAdminToken(req.header("auth-token"));
    if (auth.status != 200) {
      result = auth;
    } else {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(401).json({ errors: errors.array() });
        } else {
          result.data = await bannerModel.deleteBanner(req.params.id);
          result.status = 200;
        }
      } catch (error) {
        console.log(error);
      }
    }
    res.status(result.status).send(result);
  }
);

module.exports = router;
