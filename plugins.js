const axios = require('axios');
const elasticemail = require('elasticemail');

function toTitleCase(string = '') {
  const regex = /^[a-z]{0,1}|\s\w/gi;
  string = string.toLowerCase();
  string.match(regex).forEach((char) => {
    string = string.replace(char, char.toUpperCase());
  });
  return string;
}

function decodeHtml(str) {
  return str.replace(/&#(\d+);/g, function (match, dec) {
    return String.fromCharCode(dec);
  });
}
function formatDate(date) {
  let d = new Date(date);
  if (d instanceof Date && !isNaN(d)) {
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    let year = d.getFullYear();
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    return [year, month, day].join('-');
  }
  else {
    return '1970-01-01'
  }
}

function makeid(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

async function handleMailerAPI(url, subject, html, recipient) {
  let postRequest = {}
  postRequest = await axios({
    method: 'post',
    url: url,
    headers: {
      'accept': "application/json",
      'X-ElasticEmail-ApiKey': process.env.EMAIL_API_KEY_ELASTICEMAIL,
      'content-type': "application/json",
    },
    data: {
      "Recipients": {
        "To": [recipient],
      },
      "Content": {
        "Body": [{
          "ContentType": "HTML",
          "Content": html,
          "Charset": "utf-8",
        }],
        "From": `Mitrapabrik Team <${process.env.EMAIL_HOST}>`,
        "ReplyTo": `Mitrapabrik Team <${process.env.EMAIL_HOST}>`,
        "Subject": subject,
      },
    }
  });
  return postRequest.data
}

async function mailerElasticEmail(url, subject, html, recipient) {
 
  var client = elasticemail.createClient({
    username: process.env.EMAIL_USERNAME,
    apiKey: process.env.EMAIL_APIKEY
  });

  console.log('emmal');

  var msg = {
    from: process.env.EMAIL_HOST,
    from_name: 'Mitrapabrik Team',
    to: recipient,
    subject: subject,
    body_text: html
  };

  client.mailer.send(msg, function (err, result) {
    if (err) {
      return console.error(err);
    }

    console.log(result);
  });
}

module.exports = {
  toTitleCase,
  handleMailerAPI,
  decodeHtml,
  formatDate,
  makeid,
  mailerElasticEmail
}