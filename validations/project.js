const { body, validationResult, query } = require("express-validator");
const { onlyMessage } = require("../helpers/errorFormatter");
const IMAGE_MIME_TYPES = [
  "image/png",
  "image/jpg",
  "image/jpeg",
  "image/gif",
  "image/webp",
];

const add = [
  body("project_deadline")
    .notEmpty()
    .withMessage("project_deadline is required")
    .bail()
    .isISO8601()
    .withMessage("project_deadline must valid date")
    .bail()
    .toDate(),
  body("person_name")
    .notEmpty()
    .withMessage("person_name is required")
    .bail()
    .isLength({ min: 3, max: 100 })
    .withMessage("person_name must be 3-100 characters")
    .bail()
    .trim()
    .escape(),
  body("person_phone")
    .notEmpty()
    .withMessage("person_phone is required")
    .bail()
    .isNumeric()
    .withMessage("person_phone must be numeric")
    .bail()
    .isLength({ min: 9, max: 15 })
    .withMessage("person_phone must be 9-15 characters")
    .bail()
    .isMobilePhone()
    .withMessage("person_phone must be valid phone number")
    .bail()
    .trim()
    .escape(),
  body("project_name")
    .notEmpty()
    .withMessage("project_name is required")
    .bail()
    .isLength({ min: 3, max: 100 })
    .withMessage("project_name must be 3-100 characters")
    .bail()
    .trim()
    .escape(),
  body("project_address")
    .notEmpty()
    .withMessage("project_address is required")
    .bail()
    .isLength({ min: 3, max: 300 })
    .withMessage("project_address must be 3-300 characters")
    .bail()
    .trim()
    .escape(),
  body("project_city")
    .notEmpty()
    .withMessage("project_city is required")
    .bail()
    .isLength({ min: 3, max: 50 })
    .withMessage("project_city must be 3-50 characters")
    .bail()
    .trim()
    .escape(),
  body("project_note")
    .notEmpty()
    .withMessage("project_note is required")
    .bail()
    .isLength({ min: 3, max: 500 })
    .withMessage("project_note must be 3-500 characters")
    .bail()
    .trim()
    .escape(),
  body("project_status")
    .notEmpty()
    .withMessage("project_status is required")
    .bail()
    .isLength({ min: 1, max: 100 })
    .withMessage("project_status must be 1-100 characters")
    .bail()
    .trim()
    .escape(),
  body("project_photo").custom((_, { req, res, next }) => {
    if (
      !req.files ||
      !req.files.project_photo ||
      req.files.project_photo.length === 0
    ) {
      throw new Error("project_photo is required");
    }

    if (!IMAGE_MIME_TYPES.includes(req.files.project_photo.mimetype)) {
      throw new Error("project_photo must be an image");
    }
    const fileSize = req.files.project_photo.size / 1024 / 1024; // in MB
    if (fileSize > 2) {
      throw new Error("project_photo must be less than 2MB");
    }
    return true;
  }),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

const uploadPhoto = [
  body("photo").custom((_, { req, res, next }) => {
    if (!req.files || !req.files.photo || req.files.photo.length === 0) {
      throw new Error("photo is required");
    }

    if (!IMAGE_MIME_TYPES.includes(req.files.photo.mimetype)) {
      throw new Error("photo must be an image");
    }
    const fileSize = req.files.photo.size / 1024 / 1024; // in MB
    if (fileSize > 2) {
      throw new Error("photo must be less than 2MB");
    }
    return true;
  }),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

// all removeEventListener.params is nullable, only validate if it is not null
// limit, offset, search, sort, order
const list = [
  query("limit")
    .optional()
    .isInt({ min: 1, max: 100 })
    .withMessage("limit must be 1-100")
    .bail()
    .toInt(),
  query("offset")
    .optional()
    .isInt({ min: 0 })
    .withMessage("offset must be 0 or more")
    .bail()
    .toInt(),
  query("search")
    .optional()
    .isLength({ min: 3, max: 100 })
    .withMessage("search must be 3-100 characters")
    .bail()
    .trim()
    .escape(),
  query("sort")
    .optional()
    .isIn(["id", "project_name", "status", "created_at", "updated_at"])
    .withMessage(
      "sort must be id, project_name, status, created_at, updated_at"
    )
    .bail()
    .trim()
    .escape(),
  query("order")
    .optional()
    .isIn(["asc", "desc"])
    .withMessage("order must be asc or desc")
    .bail()
    .trim()
    .escape(),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

const reply = [
  body("project_id")
    .notEmpty()
    .withMessage("project_id is required")
    .bail()
    .isInt({ min: 1 })
    .withMessage("project_id must be 1 or more")
    .bail()
    .toInt(),
  body("reply_id")
    .notEmpty()
    .withMessage("reply_id is required")
    .bail()
    .isInt({ min: 1 })
    .withMessage("reply_id must be 1 or more")
    .bail()
    .toInt(),
];

const updateStatus = [
  body("id")
    .notEmpty()
    .withMessage("id is required")
    .bail()
    .isInt({ min: 1 })
    .withMessage("id must be 1 or more")
    .bail()
    .toInt(),
  body("status")
    .notEmpty()
    .withMessage("status is required")
    .bail()
    .isIn(["pending", "accepted", "rejected"])
    .withMessage("status must be pending, accepted, rejected")
    .bail(),
];

const deleteProject = [
  body("id").notEmpty().isNumeric().trim().escape(),
  (req, res, next) => {
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

module.exports = { add, uploadPhoto, list, reply, updateStatus, deleteProject };
