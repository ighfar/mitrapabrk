const { body, validationResult } = require("express-validator");
const { onlyMessage } = require("../helpers/errorFormatter");
const IMAGE_MIME_TYPES = [
  "image/png",
  "image/jpg",
  "image/jpeg",
  "image/gif",
  "image/webp",
];

const add = [
  body("type")
    .notEmpty()
    .withMessage("type is required")
    .bail()
    .isLength({ min: 3, max: 50 })
    .withMessage("type must be between 3 and 50 characters")
    .bail()
    .trim()
    .escape(),
  body("brand")
    .notEmpty()
    .withMessage("brand is required")
    .bail()
    .isLength({ min: 3, max: 50 })
    .withMessage("brand must be 3-50 characters")
    .bail()
    .trim()
    .escape(),
  body("material")
    .notEmpty()
    .withMessage("material is required")
    .bail()
    .isLength({ min: 3, max: 200 })
    .withMessage("material must be 3-200 characters")
    .bail()
    .trim()
    .escape(),
  body("area_needed")
    .notEmpty()
    .withMessage("area_needed is required")
    .bail()
    .isNumeric()
    .withMessage("area_needed must be numeric")
    .bail(),
  body("size")
    .notEmpty()
    .withMessage("size is required")
    .bail()
    .isNumeric()
    .withMessage("size must be numeric")
    .bail(),
  body("quantity")
    .notEmpty()
    .withMessage("quantity is required")
    .bail()
    .isNumeric()
    .withMessage("quantity must be numeric")
    .bail(),
  body("budget")
    .notEmpty()
    .withMessage("budget is required")
    .bail()
    .isNumeric()
    .withMessage("budget must be numeric")
    .bail(),
  body("project_id")
    .notEmpty()
    .withMessage("project_id is required")
    .bail()
    .isNumeric()
    .withMessage("project_id must be numeric")
    .bail(),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
  body("photo").custom((_, { req, res, next }) => {
    if (!req.files || !req.files.photo || req.files.photo.length === 0) {
      throw new Error("photo is required");
    }

    if (!IMAGE_MIME_TYPES.includes(req.files.photo.mimetype)) {
      throw new Error("photo must be an image");
    }
    const fileSize = req.files.photo.size / 1024 / 1024; // in MB
    if (fileSize > 2) {
      throw new Error("photo must be less than 2MB");
    }
    return true;
  }),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

const uploadPhoto = [
  body("photo").custom((_, { req, res, next }) => {
    if (!req.files || !req.files.photo || req.files.photo.length === 0) {
      throw new Error("photo is required");
    }

    if (!IMAGE_MIME_TYPES.includes(req.files.photo.mimetype)) {
      throw new Error("photo must be an image");
    }
    const fileSize = req.files.photo.size / 1024 / 1024; // in MB
    if (fileSize > 2) {
      throw new Error("photo must be less than 2MB");
    }
    return true;
  }),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

module.exports = { add, uploadPhoto };
