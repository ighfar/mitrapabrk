const { body, validationResult } = require("express-validator");
const { onlyMessage } = require("../helpers/errorFormatter");

const verify = [
  body("hash")
    .notEmpty()
    .withMessage("hash is required")
    .bail()
    .isLength({ min: 3, max: 300 })
    .withMessage("hash must be 3-300 characters")
    .bail()
    .trim()
    .escape(),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

module.exports = {
  verify,
};
