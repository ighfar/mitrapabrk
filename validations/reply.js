const { body, validationResult, query, param } = require("express-validator");
const { onlyMessage } = require("../helpers/errorFormatter");

const add = [
  body("message")
    .notEmpty()
    .withMessage("message is required")
    .bail()
    .isLength({ min: 3, max: 300 })
    .withMessage("message must be 3-300 characters")
    .bail()
    .trim()
    .escape(),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

const get = [
  param("id")
    .notEmpty()
    .withMessage("id is required")
    .bail()
    .isNumeric()
    .withMessage("id must be numeric")
    .bail()
    .isLength({ min: 1, max: 11 })
    .withMessage("id must be 1-11 characters")
    .bail()
    .trim()
    .escape(),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

const update = [
  param("id")
    .notEmpty()
    .withMessage("id is required")
    .bail()
    .isNumeric()
    .withMessage("id must be numeric")
    .bail()
    .isLength({ min: 1, max: 11 })
    .withMessage("id must be 1-11 characters")
    .bail()
    .trim()
    .escape(),
  body("message")
    .notEmpty()
    .withMessage("message is required")
    .bail()
    .isLength({ min: 3, max: 300 })
    .withMessage("message must be 3-300 characters")
    .bail()
    .trim()
    .escape(),
  (req, res, next) => {
    const errors = validationResult(req).formatWith(onlyMessage);
    if (!errors.isEmpty())
      return res.status(422).json({ errors: errors.array() });
    next();
  },
];

module.exports = {
  add,
  get,
  update,
};
