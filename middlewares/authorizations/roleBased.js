const { isValidToken } = require("../../helpers/auth");

const roles = (permittedRoles) => {
  return (req, res, next) => {
    if (typeof roles === "string") {
      roles = [roles];
    }
    const token = req.header("auth-token");
    const key = process.env.PRIVATE_TOKEN_KEY;
    if (!token || !isValidToken(key, token)) {
      return res.status(401).send({ message: "Unauthorized!" });
    }
    const user = isValidToken(key, token);
    if (permittedRoles.includes(user.role)) {
      next();
    } else {
      return res.status(403).send({ message: "Forbidden!" });
    }
  };
};

module.exports = {
  roles,
};
