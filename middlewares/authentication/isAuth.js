const { isValidToken } = require("../helpers/auth");

const isAuthenticated = (req, res, next) => {
  const token = req.header("auth-token");
  const key = process.env.PRIVATE_TOKEN_KEY;
  if (!token || !isValidToken(key, token)) {
    return res.status(401).send({ message: "Unauthorized!" });
  }
  next();
};

module.exports = { isAuthenticated };
