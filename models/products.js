require('dotenv').config();
const DB = require('./index')

async function getAllProducts(search, orderBy = 'id_produk', orderMethod = 'desc', perPage = 10, currentPage, kategoriId) {
    const query = DB.knex('products').select('products.*', DB.knex.raw('(select group_concat(t.path) from product_images t where t.product_id=products.id_produk) as images')).orderBy(orderBy, orderMethod);

    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("tipe", "like", `${search}%`)
            .orWhere("penempatan", "like", `%${search}%`);
    }

    if (kategoriId) {
        query.where("kategori_id", kategoriId)
    }

    return await query.paginate({
        perPage: perPage,
        currentPage: currentPage
    });
}

async function getAllProductsSale() {
    return await DB.knex('products').select('products.*', DB.knex.raw('(select group_concat(t.path) from product_images t where t.product_id=products.id_produk) as images')).where('sale', 1);
}

async function getTotalProducts(search) {
    const query = DB.knex('products');

    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("tipe", "like", `${search}%`)
            .orWhere("penempatan", "like", `%${search}%`);
    }

    return await query.count("id_produk", { as: 'total' });
}

async function getWishlistProducts(search, orderBy = 'id_produk', orderMethod = 'desc', perPage = 10, currentPage, kategoriId, userId) {

    console.log(userId);

    const query = DB.knex('products')
                .join("wishlist", "products.id_produk", "=", "wishlist.id_product")
                .where("wishlist.id_user", userId)
                .orderBy(orderBy, orderMethod);

    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("tipe", "like", `${search}%`)
            .orWhere("penempatan", "like", `%${search}%`);
    }

    if (kategoriId) {
        query.where("kategori_id", kategoriId)
    }

    return await query.paginate({
        perPage: perPage,
        currentPage: currentPage
    });
}

async function getWishlistTotalProducts(search, userId) {
    const query = DB.knex('products')
                    .join("wishlist", "products.id_produk", "=", "wishlist.id_product")
                    .where("wishlist.id_user", userId);

    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("tipe", "like", `${search}%`)
            .orWhere("penempatan", "like", `%${search}%`);
    }

    return await query.count("id_produk", { as: 'total' });
}

async function getAllProductsOther(productId) {
    return await DB.knex('products').whereIn('id_partner', DB.knex.raw(`select id_partner from products where id_produk=${productId}`)).orderByRaw('RAND()').limit(8);
}

async function getAllMaterials(project_id) {
    return await DB.knex('materials').where(project_id).limit(100)
}
async function getAllKategories() {
    const kategori = await DB.knex.select('Id', 'kategori', 'parent_id')
    .from(DB.knex.raw('kategori')).where('parent_id', 0);

    return kategori;
}

async function getAllChildKategories(parentId) {
    const kategori = await DB.knex.select('Id', 'kategori', 'parent_id')
    .from(DB.knex.raw('kategori')).where('parent_id', parentId);

    return kategori;
}

async function getSubCategories() {
    return await DB.knex('kategori as a')
                    .select(
                        "a.Id",
                        DB.knex.raw(`CONCAT(b.kategori," -> ", a.kategori) as "kategori"`))
                    .join("kategori as b", "a.parent_id", "=", "b.Id")
}

async function getAllRawCategories() {
    return await DB.knex('kategori as a')
                    .select(
                        "a.Id as value",
                        DB.knex.raw(`CONCAT(b.kategori," -> ", a.kategori) as "text"`))
                    .join("kategori as b", "a.parent_id", "=", "b.Id")
}

async function getAllProductsByType(tipe) {
    return await DB.knex('products').where('tipe','=',tipe)
}

async function getAllProductsByPartner(id_partner) {
    return await DB.knex('products').where('status','=',1).andWhere({id_partner})
}

async function getProductDetail(id_produk) {
    return await DB.knex.select(
        'p.*', 
        DB.knex.raw(`(select concat(a.kategori, ' -> ', b.kategori) as kategori from kategori a inner join kategori b on a.parent_id = b.Id where a.Id=p.kategori_id) as "kategori"`),
        'u.nama_perusahaan', 
        'u.asal_kota', 
        'u.no_hp', 
        'u.foto_perusahaan')
    .from(DB.knex.raw('products p, user u'))
    .where(DB.knex.raw(`p.id_partner = u.id_user and p.id_produk = ${id_produk} and p.status != -1`))
}

async function getImagesProduct(product_id) {
    return await DB.knex('product_images').where({product_id})
}

async function getProductById(id_produk) {
    return await DB.knex('products').where({id_produk})
}

async function getProductType() {
    return await DB.knex.select('tipe')
    .from(DB.knex.raw('products'))
    .groupBy(DB.knex.raw(`tipe`))
}
async function getProductTypeByUser(user_id) {
    return await DB.knex.select('tipe')
    .from(DB.knex.raw('products'))
    .where(DB.knex.raw(`user_id = ${user_id}`))
}
async function addNewProduct(data) {
    let result = await DB.knex('products').insert(data)
    return result
}

async function editProduct(id_produk, id_partner, data) {
    console.log(data)
    let result = await DB.knex('products').where({id_produk, id_partner}).update(data).then();
    console.log(result)
}

async function deleteProduct(id_produk, id_partner) {
    let result = await DB.knex('products').where({id_produk, id_partner}).del().then();
    console.log(result)
}

async function deleteProductByPartner(id_partner) {
    let result = await DB.knex('products').where({id_partner}).del().then();
}

async function adminDeleteProduct(id_produk) {
    let result = await DB.knex('products').where({id_produk}).del().then();
}

async function addProductImage(data) {
    return await DB.knex('product_images').insert(data)
}

async function findProductImage(id) {
    return await DB.knex('product_images').where({id}).first();
}

async function deleteProductImage(id) {
    return await DB.knex('product_images').where({id}).del().then();
}

module.exports = {
    getAllProducts,
    getAllProductsByPartner,
    getAllKategories,
    getAllProductsByType,
    getProductDetail,
    getProductType,
    getProductTypeByUser,
    addNewProduct,
    editProduct,
    deleteProduct,
    getTotalProducts,
    adminDeleteProduct,
    getProductById,
    deleteProductByPartner,
    getAllRawCategories,
    getAllChildKategories,
    getAllProductsSale,
    getWishlistProducts,
    getWishlistTotalProducts,
    getAllProductsOther,
    getSubCategories,
    getImagesProduct,
    addProductImage,
    findProductImage,
    deleteProductImage
}