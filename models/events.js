require('dotenv').config();
const DB = require('./index')

async function getAllEvents(search, orderBy = 'id_event', orderMethod = 'desc', perPage = 10, currentPage) {

    const query = DB.knex('event').orderBy(orderBy, orderMethod);

    if (search) {
        query.where("event_name", "like", `${search}%`)
            .orWhere("body", "like", `%${search}%`);
    }

    return await query.paginate({
        perPage: perPage,
        currentPage: currentPage
    });

}

async function getTotalAllEvents(search) {
    const query = DB.knex('event');
    if (search) {
        query.where("event_name", "like", `${search}%`)
            .orWhere("body", "like", `%${search}%`);
    }
    return await query.count("id_event", { as: 'total' });
}

async function getAllPublishEvents() {
    return await DB.knex('event').where('status', '=', 1).orderBy("id_event", "desc")
}

async function getEventData(id_event) {
    return await DB.knex.select('ea.*', 'e.event_name', 'e.starting_date', 'e.author', 'e.location', 'e.admission', 'e.image', 'e.body', 'e.status', 'u.email', 'u.nama_depan', 'u.nama_belakang')
    .from(DB.knex.raw('event_attendance ea, event e, user u'))
    .where(DB.knex.raw(`ea.id_user = u.id_user and e.id_event = ea.id_event and ea.id_event = '${id_event}'`)).first();
}

async function getFirstEvent(id_event) {
    return await DB.knex('event').where({ id_event }).first();
}

async function addEvent(data) {
    let result = await DB.knex('event').insert(data)
}

async function editEvent(id_event, data) {
    let result = await DB.knex('event').where({ id_event }).update(data).then();
}

async function deleteEvent(id_event) {
    let result = await DB.knex('event').where({ id_event }).del().then();
}

async function getEventAttendanceByEvent(id_event) {
    return await DB.knex.select('ea.*', 'e.event_name', 'e.starting_date', 'e.author', 'e.location', 'e.admission', 'u.email', 'u.nama_lengkap', 'u.no_hp')
        .from(DB.knex.raw('event_attendance ea, event e, user u'))
        .where(DB.knex.raw(`ea.id_user = u.id_user and e.id_event = ea.id_event and ea.id_event = '${id_event}'`)).orderByRaw(DB.knex.raw('ea.attendance desc, u.nama_lengkap asc'))
}

async function getEventDataByUser(id_user, id_event) {
    return await DB.knex.select('ea.*', 'e.event_name', 'e.starting_date', 'e.author', 'e.location', 'e.admission', 'u.email', 'u.nama_lengkap', 'u.no_hp')
    .from(DB.knex.raw('event_attendance ea, event e, user u'))
    .where(DB.knex.raw(`ea.id_user = u.id_user and e.id_event = ea.id_event and ea.id_event = '${id_event}' and ea.id_user = '${id_user}'`)).first();
}

async function getEventAttendanceByUser(id_user, id_event) {
    return await DB.knex('event_attendance').where({ id_user, id_event }).first();
}

async function registerEvent(data) {
    let result = await DB.knex('event_attendance').insert(data)
}

async function confirmAttendance(id_trans) {
    await DB.knex('event_attendance').where({ id_trans }).update({ attendance: 1 }).then()
    return await DB.knex.select('ea.id_trans', 'e.event_name', 'e.starting_date', 'u.email', 'u.nama_depan', 'u.nama_belakang', 'u.no_hp')
        .from(DB.knex.raw('event_attendance ea, user u, event e'))
        .where(DB.knex.raw(`ea.id_event = e.id_event and ea.id_user = u.id_user and ea.id_trans = '${id_trans}'`))
}

module.exports = {
    getAllEvents,
    getEventData,
    addEvent,
    editEvent,
    deleteEvent,
    getEventAttendanceByEvent,
    getEventAttendanceByUser,
    registerEvent,
    confirmAttendance,
    getAllPublishEvents,
    getTotalAllEvents,
    getFirstEvent,
    getEventDataByUser
}