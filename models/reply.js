async function addReply(data) {
  const insert = await DB.knex("replys").insert(data);
  return { id: insert[0] };
}

async function getReplysByProject(project_id) {
  const query = DB.knex("replys")
    .select("replys.*", "projects.*")
    .join("projects", "replys.project_id", "=", "projects.id")
    .where("replys.project_id", project_id);
  return await query;
}

async function updateReply(id, data) {
  const { message } = data;
  const update = await DB.knex("replys").where({ id }).update({ message });
  return update;
}

module.exports = {
  addReply,
  getReplysByProject,
  updateReply,
};
