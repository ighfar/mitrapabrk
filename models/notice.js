require('dotenv').config();
const DB = require('./index')
const plugins = require('../plugins')

async function getAllNotice() {
    return await DB.knex('notice').select(
        "id_notice",
        "message",
        "link",
        "timestamp as createat",
        "publish as broadcast"
    ).orderBy('timestamp','desc').where("publish", 0)
}

async function pushNotice(data) {
    data.timestamp = new Date(Date.now())
    let result = await DB.knex('notice').insert(data)
}

async function update(id, data) {
    data.timestamp = new Date(Date.now())
    let result = await DB.knex('notice').where('id_notice', id).update(data)
}

async function checkLink(link) {
   return await DB.knex('notice').where('link', link).first()
}

async function checkMessage(message) {
    return await DB.knex('notice').where('message', message).first()
 }

async function deleteNotice(id_notice) {
    let result = await DB.knex('notice').where({id_notice}).del().then();
}

module.exports = {
    getAllNotice,
    pushNotice,
    deleteNotice,
    update,
    checkLink,
    checkMessage
}