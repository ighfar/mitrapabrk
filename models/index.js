require("dotenv").config();

const ENVIRONMENT =
  process.env.NODE_ENV || process.env.ENVIRONMENT || "production";

const DATABASE_CREDENTIAL = {
  test: {
    host: process.env.DB_TEST_HOST,
    port: process.env.DB_TEST_PORT,
    user: process.env.DB_TEST_USER,
    password: process.env.DB_TEST_PASSWORD,
    database: process.env.DB_TEST_SCHEMA,
  },
  development: {
    host: process.env.DB_DEV_HOST,
    port: process.env.DB_DEV_PORT,
    user: process.env.DB_DEV_USER,
    password: process.env.DB_DEV_PASSWORD,
    database: process.env.DB_DEV_SCHEMA,
  },
  production: {
    host: process.env.DB_PROD_HOST,
    port: process.env.DB_PROD_PORT,
    user: process.env.DB_PROD_USER,
    password: process.env.DB_PROD_PASSWORD,
    database: process.env.DB_PROD_SCHEMA,
  },
};

const knex = require("knex")({
  client: "mysql2",
  connection: DATABASE_CREDENTIAL[ENVIRONMENT],
  acquireConnectionTimeout: 10000,
  // pool: { min: 0, max: 100 }
});

const { attachPaginate } = require('knex-paginate');
attachPaginate();

// if (!knex.pool) throw new Error("Database not connected");

module.exports = {
  knex,
};
