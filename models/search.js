require('dotenv').config();
const DB = require('./index')

async function searchProduct(limit, offset, searchString) {
    let search = `${searchString}`.toString().toLowerCase()
    return await DB.knex.select(DB.knex.raw('p.*, u.nama_perusahaan, COUNT(*) OVER() as total_rows'))
    .from(DB.knex.raw('products p, user u'))
    .where(DB.knex.raw(`p.status = 1 and p.id_partner = u.id_user and
	(
        lower(p.title) like lower('%${search}%') or
        lower(p.varian) like lower('%${search}%') or
        lower(p.deskripsi) like lower('%${search}%') or 
        lower(u.nama_perusahaan) like lower('%${search}%')
    )`))
    .limit(limit).offset(offset)
    .orderBy('p.title')
}

async function searchArtikel(searchString) {
    return await DB.knex.select('id_artikel','title','tipe').from('artikel').where('status','=',1).andWhere(DB.knex.raw(`lower(title) like '%${searchString}%'`)).limit(5)
}

async function searchEvent(searchString) {
    return await DB.knex.select('id_event','event_name','starting_date').from('event').where('status','=',1).andWhere(DB.knex.raw(`lower(event_name) like '%${searchString}%'`)).limit(5)
}

async function searchShowroom(searchString) {
    return await DB.knex.select('id_showroom','title','kota').from('showroom').where('status','=',1).andWhere(DB.knex.raw(`lower(title) like '%${searchString}%'`)).limit(5)
}

module.exports = {
    searchProduct,
    searchArtikel,
    searchEvent,
    searchShowroom,
}