require('dotenv').config();
const DB = require('./index')

async function getAll(search, orderBy = 'id', orderMethod = 'desc', perPage = 10, currentPage) {

    const query = DB.knex('banner').orderBy(orderBy, orderMethod);

    if (search) {
        query.where("title", "like", `%${search}%`);
    }

    return await query.paginate({
        perPage: perPage,
        currentPage: currentPage
    });

}

async function getTotal(search) {
    const query = DB.knex('banner');
    if (search) {
        query.where("title", "like", `%${search}%`);
    }
    return await query.count("id", { as: 'total' });
}


async function getFirst(id) {
    return await DB.knex('banner').where({ id }).first();
}

async function add(data) {
    return await DB.knex('banner').insert(data)
}

async function edit(id, data) {
    return await DB.knex('banner').where({ id }).update(data).then();
}

async function deleteBanner(id) {
    return await DB.knex('banner').where({ id }).del().then();
}

async function getPublish() {
    return await DB.knex('banner');
}

module.exports = {
    getAll,
    getTotal,
    add,
    edit,
    deleteBanner,
    getFirst,
    getPublish
}