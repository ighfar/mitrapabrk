const DB = require("./index");

async function addMaterial(data) {
  const insert = await DB.knex("materials").insert(data);
  return { id: insert[0] };
}

async function updateMaterial(id, data) {
  const update = await DB.knex("materials").where({ id }).update(data);
  return update;
}
async function getProjectDetail(id) {
    return await DB.knex.select('p.*', 'm.*')
    .from(DB.knex.raw('projects p, materials m'))
    .where(DB.knex.raw(`p.id = m.project_id`))
    .groupBy('project_id');
}

async function deleteMaterialByProject(project_id) {
  return await DB.knex('materials').where({project_id}).del().then();
}

module.exports = {
  addMaterial,
  updateMaterial,
  getProjectDetail,
  deleteMaterialByProject
};
