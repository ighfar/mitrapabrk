require('dotenv').config();
const DB = require('./index')

async function getAllShowrooms() {
    let result = await DB.knex('showroom').where('status','!=',-1)
    let user = await DB.knex('user').where('nama_perusahaan','!=','null')

    let fetchShowroomKota = await DB.knex('showroom').distinct('kota')
    let listKota = {}
    fetchShowroomKota.forEach(row => {
        listKota[row.kota] = []
    });

    result.forEach(row => {
        let tmpBrand = row.list_brand.split(',')
        row.list_brand = []
        tmpBrand.forEach(id => {
            let findUser = user.find(row => row.id_user == id)
            row.list_brand.push({
                id,
                nama_perusahaan:findUser.nama_perusahaan,
                foto_perusahaan:findUser.foto_perusahaan,
            })
        });
        listKota[row.kota].push(row)
    });

    return listKota
}
async function getAllShowroom() {
    return await DB.knex('showroom').where('status','=', 1)
}

async function getAllShowroomByCity() {
    return await DB.knex('showroom').select(
            "kota", 
            DB.knex.raw(`MAX(image) as "image"`)
            ).where('status','=', 1).groupBy('kota');
}

async function findByCity(kota) {
    return await DB.knex('showroom').where('kota','=', kota);
}

async function getAllShowroomList(search, orderBy = 'id_showroom', orderMethod = 'desc', perPage = 10, currentPage) {
    const query = DB.knex('showroom').orderBy(orderBy, orderMethod);

    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("alamat", "like", `%${search}%`)
            .orWhere("kota", "like", `${search}%`);
    }

    return await query.paginate({
        perPage: perPage,
        currentPage: currentPage
    });
}

async function getTotalShowrooms(search) {
    const query = DB.knex('showroom');

    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("alamat", "like", `%${search}%`)
            .orWhere("kota", "like", `${search}%`);
    }

    return await query.count("id_showroom", { as: 'total' });
}

async function getShowroomById(id_showroom) {
    return await DB.knex('showroom').where('id_showroom', id_showroom)
}

async function getShowroomByIdFirst(id_showroom) {
    return await DB.knex('showroom').where('id_showroom', id_showroom).first();
}

async function getPrincipal(arrayId) {
    return await DB.knex('user').select('foto_perusahaan').whereIn('id_user', arrayId);
}

async function addNewShowroom(data) {
    return await await DB.knex('showroom').insert(data)
}

async function updateShowroom(id_showroom, data) {
    return await await DB.knex('showroom').where({id_showroom}).update(data).then();
}

async function deleteShowroom(id_showroom) {
    return await await DB.knex('showroom').where({id_showroom}).del().then();
}

module.exports = {
    getAllShowrooms,
    getAllShowroom,
    addNewShowroom,
    updateShowroom,
    deleteShowroom,
    getShowroomById,
    getAllShowroomList,
    getTotalShowrooms,
    getAllShowroomByCity,
    findByCity,
    getShowroomByIdFirst,
    getPrincipal
}