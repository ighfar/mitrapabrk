const DB = require("./index");

async function addProject(data) {
  const insert = await DB.knex("projects").insert(data);
  return { id: insert[0] };
}

async function listProjects(search, orderBy = 'id', orderMethod = 'desc', perPage = 10, currentPage) {

  const query = DB.knex("projects")
    .select(
      "projects.*",
      DB.knex.raw(`CONCAT('[',GROUP_CONCAT(JSON_OBJECT(
      'id', materials.id,
      'type', materials.type,
      'brand', materials.brand,
      'material', materials.material,
      'area_needed', materials.area_needed,
      'size', materials.size,
      'quantity', materials.quantity,
      'budget', materials.budget,
      'photo', materials.photo,
      'is_matched', materials.is_matched,
      'user_id', materials.user_id,
      'project_id', materials.project_id
  )),']') as materials`)
    )
    .leftJoin("materials", "projects.id", "=", "materials.project_id")
    .groupBy("projects.id");

  if (search) {
    query.where("projects.person_name", "like", `${search}%`)
    .orWhere("projects.project_name", "like", `${search}%`)
    .orWhere("projects.project_city", "like", `${search}%`);
  }

  query.orderBy("projects." + orderBy, orderMethod);

  return await query.paginate({
    perPage: perPage,
    currentPage: currentPage
  });
}

async function getUserProjects(userId, search, orderBy = 'id', orderMethod = 'desc', perPage = 10, currentPage) {

  const query = DB.knex("projects").where("user_id", "=", userId).orderBy(orderBy, orderMethod);

  if (search) {
    query.where("person_name", "like", `${search}%`)
      .orWhere("project_name", "like", `${search}%`)
      .orWhere("project_city", "like", `${search}%`);
  }

  return await query.paginate({
    perPage: perPage,
    currentPage: currentPage
  });
}

async function getUserProjectsFirst(userId) {
  return await DB.knex("projects").where("user_id", "=", userId).first();
}

async function getTotalUserProjects(userId, search) {
  const query = DB.knex("projects").where("user_id", "=", userId);
  if (search) {
    query.where("person_name", "like", `${search}%`)
      .orWhere("project_name", "like", `${search}%`)
      .orWhere("project_city", "like", `${search}%`);
  }
  return await query.count("id", { as: 'total' });
}

async function getAdminProjects(search, orderBy = 'id', orderMethod = 'desc', perPage = 10, currentPage) {

  const query = DB.knex('projects').orderBy(orderBy, orderMethod);

  if (search) {
    query.where("person_name", "like", `${search}%`)
      .orWhere("project_name", "like", `${search}%`)
      .orWhere("project_city", "like", `${search}%`);
  }

  return await query.paginate({
    perPage: perPage,
    currentPage: currentPage
  });
}

async function getTotalProjects(search) {

  const query = DB.knex('projects');

  if (search) {
    query.where("person_name", "like", `${search}%`)
      .orWhere("project_name", "like", `${search}%`)
      .orWhere("project_city", "like", `${search}%`);
  }

  return await query.count("id", { as: 'total' });
}

async function detailProject(id) {

  const query = DB.knex("projects")
    .select(
      "projects.*",
      DB.knex.raw(`CONCAT('[',GROUP_CONCAT(JSON_OBJECT(
      'id', materials.id,
      'type', materials.type,
      'brand', materials.brand,
      'material', materials.material,
      'area_needed', materials.area_needed,
      'size', materials.size,
      'quantity', materials.quantity,
      'budget', materials.budget,
      'photo', materials.photo,
      'is_matched', materials.is_matched,
      'user_id', materials.user_id,
      'project_id', materials.project_id
  )),']') as materials`)
    )
    .leftJoin("materials", "projects.id", "=", "materials.project_id")
    .where(DB.knex.raw(`projects.id = ${id}`))
    .groupBy("projects.id");


  return await query;
}

async function getProjectByIdFirst(id) {

  return await DB.knex("projects").where({ id }).first();

}

async function getProjectByName(project_name) {

  return await DB.knex("projects").where({ project_name }).first();

}

async function getProjectsById(id) {


  // join to project table ON materials.project_id = project.id

  return await DB.knex.select('p.*', 'm.*')
    .from(DB.knex.raw('projects p, materials m'))
    // .on(DB.knex.raw(`p.id = m.project_id`))
    .where(DB.knex.raw(`p.id = m.project_id and p.id = ${id}`))



}
async function updateProject(id, data) {
  if (!id) {
    throw new Error("id is required");
  }
  const update = await DB.knex("projects").where({ id }).update(data);
  return update;
}

// reply projects with relation many to many between projects and replys
// with projects_replys mapping table with foreign key project_id to proejcts.id
// and reply_id to replys.id

async function replyProject(data) {
  const { project_id, reply_id } = data;

  if (!project_id || !reply_id) {
    throw new Error("project_id and reply_id are required");
  }

  const insert = await DB.knex("projects_replys").insert(data);
  return { id: insert[0] };
}

async function deleteProject(id) {
  let result = await DB.knex('projects').where({ id }).del().then();
}
async function getProjectDetail(id) {
  return await DB.knex.select('p.*', 'm.*')
    .from(DB.knex.raw('projects p, materials m'))
    .where(DB.knex.raw(`p.id = m.project_id`))
    .groupBy('project_id');
}

module.exports = {
  addProject,
  detailProject,
  getProjectsById,
  getProjectDetail,
  getAdminProjects,
  listProjects,
  updateProject,
  replyProject,
  deleteProject,
  getUserProjects,
  getTotalProjects,
  getProjectByIdFirst,
  getTotalUserProjects,
  getProjectByName,
  getUserProjectsFirst
};
