require('dotenv').config();
const DB = require('./index')
const plugins = require('../plugins')

async function getAllrequests() {
    return await DB.knex('requests').where('status','!=',-1)
}

async function getRequestByUser(id_user) {
    return await DB.knex('requests').where({id_user})
}

async function addNewRequest(data) {
    data.timestamp = plugins.formatDate(Date.now())
    let result = await DB.knex('requests').insert(data)
    console.log(result)
}

async function finishRequest(id_request) {
    let result = await DB.knex('requests').where({id_request}).update({status:1}).then();
    console.log(result)
}

module.exports = {
    getAllrequests,
    getRequestByUser,
    addNewRequest,
    finishRequest,
}