require("dotenv").config();
const DB = require("./index");
const CryptoJS = require("crypto-js/aes");
const ENCODER_TYPE = require("crypto-js/enc-utf8");
const plugins = require('../plugins');

function checkPassword(pass, compare) {
  let bytes = CryptoJS.decrypt(pass, process.env.SECRET_KEY);
  let realPass = bytes.toString(ENCODER_TYPE);
  if (realPass == compare) return true;
  else return false;
}

async function login(email, password) {
  let userData = await DB.knex("user").where({ email: email });
  if (userData.length > 0) {
    if (checkPassword(userData[0].password, password)) {
      return {
        status: true,
        id_user: userData[0].id_user,
        email: userData[0].email,
        nama_lengkap: userData[0].nama_lengkap,
        nama_depan: userData[0].nama_depan,
        nama_belakang: userData[0].nama_belakang,
        role: userData[0].role,
        user_status: userData[0].status,
        pocketbase_id: userData[0].pocketbase_id,
      };
    } else {
      return {
        status: false,
        err: {
          value: password,
          msg: "Password salah.",
          param: "password",
          location: "body",
        },
      };
    }
  } else {
    return {
      status: false,
      err: {
        value: email,
        msg: "Email tidak valid.",
        param: "email",
        location: "body",
      },
    };
  }
}

async function loginSementara(email, password_sementara) {
  let userData = await DB.knex("user").where({ email: email });
  if (userData.length > 0) {
    if (checkPassword(userData[0].password_sementara, password_sementara)) {
      return {
        status: true,
        id_user: userData[0].id_user,
        email: userData[0].email,
        nama_lengkap: userData[0].nama_lengkap,
        nama_depan: userData[0].nama_depan,
        nama_belakang: userData[0].nama_belakang,
        role: userData[0].role,
        user_status: userData[0].status,
      };
    } else {
      return {
        status: false,
        err: {
          value: password_sementara,
          msg: "Password salah.",
          param: "password",
          location: "body",
        },
      };
    }
  } else {
    return {
      status: false,
      err: {
        value: email,
        msg: "Email tidak valid.",
        param: "email",
        location: "body",
      },
    };
  }
}

async function register(data) {
  let checkEmail = await DB.knex("user").where({ email: data.email });
  if (checkEmail.length <= 0) {
    let encryptedPass = CryptoJS.encrypt(
      data.password,
      process.env.SECRET_KEY
    ).toString();

    data.timestamp = plugins.formatDate(Date.now());
    data.password = encryptedPass;

    let result = await DB.knex("user").insert(data);
    return { status: true, data: result };
  } else {
    return {
      status: false,
      err: {
        value: data.email,
        msg: "Email sudah terdaftar.",
        param: "email",
        location: "body",
      },
    };
  }
}

async function forgotPass(data) {
  let checkEmail = await DB.knex("user").where({ email: data.email });
  if (checkEmail.length >= 1) {
    let encryptedPass = CryptoJS.encrypt(
      data.password,
      process.env.SECRET_KEY
    ).toString();
    data.password = encryptedPass;
    let result = await DB.knex("user").insert(data);
    return { status: true, data: result };
  } else {
    return {
      status: false,
      err: {
        value: data.email,
        msg: "Email sudah terdaftar.",
        param: "email",
        location: "body",
      },
    };
  }
}

async function updatePassword(id_user, newpassword) {
  let encryptedPass = CryptoJS.encrypt(
    newpassword.toString(),
    process.env.SECRET_KEY
  ).toString();
  try {
    await DB.knex("user")
      .where({ id_user })
      .update({ password: encryptedPass })
      .then();
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}

async function forgetPassword(t, newpass) {
  let encryptedPass = CryptoJS.encrypt(
    newpass,
    process.env.SECRET_KEY
  ).toString();
  try {
    await DB.knex("user")
      .where({ t })
      .update({ password: encryptedPass })
      .then();
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}


async function updatedataDiri(data, id_user) {
  let userData = await DB.knex("user").where({ id_user });
  if (userData.length > 0) {
    await DB.knex("user").where({ id_user }).update(data).then();
    return { status: true };
  } else {
    return {
      status: false,
      err: {
        value: id_user,
        msg: "Id user tidak valid.",
        param: "id_user",
        location: "body",
      },
    };
  }
}

async function adminLogin(id_admin, password) {
  let userData = await DB.knex("admin").where({ id_admin: id_admin });
  if (userData.length > 0) {
    if (checkPassword(userData[0].password, password)) {
      return {
        status: true,
        id_admin: userData[0].id_admin,
        nama: userData[0].nama,
        level: userData[0].level,
      };
    } else {
      return {
        status: false,
        err: {
          value: password,
          msg: "Password salah.",
          param: "password",
          location: "body",
        },
      };
    }
  } else {
    return {
      status: false,
      err: {
        value: id_admin,
        msg: "id_admin tidak valid.",
        param: "id_admin",
        location: "body",
      },
    };
  }
}

async function adminRegister(data) {
  let checkEmail = await DB.knex("admin").where({ id_admin: data.id_admin });
  if (checkEmail.length <= 0) {
    let encryptedPass = CryptoJS.encrypt(
      data.password,
      process.env.SECRET_KEY
    ).toString();
    data.password = encryptedPass;
    let result = await DB.knex("admin").insert(data);
    return { status: true, data: result };
  } else {
    return {
      status: false,
      err: {
        value: data.id_admin,
        msg: "id_admin sudah terdaftar.",
        param: "email",
        location: "body",
      },
    };
  }
}

async function getUserData(email) {
  return await DB.knex
    .select(
      "nama_lengkap",
      "tanggal_lahir",
      "gender",
      "alamat",
      "email",
      "role",
      "no_hp",
      "asal_kota",
      "profesi",
      "tujuan",
      "minat",
      "nama_perusahaan",
      "alamat_perusahaan",
      "nama_direktur",
      "nomer_telepone",
      "nomer_fax",
      "email_perusahaan",
      "email_secondary",
      "nomer_tdp",
      "tipe_barang",
      "foto_perusahaan",
      "foto_profil",
      "gallery",
      "portofolio"
    )
    .from("user")
    .where({ email });
}

async function getUserPassword(email) {
  return await DB.knex.select("nama_lengkap", "nama_depan", "nama_belakang", "password", "password_nohash").from("user").where({ email });
}

async function getUserByEmail(email) {
  return await DB.knex.select("id_user", "nama_lengkap").from("user").where({ email }).first();
}

async function getUserGalleries(id_user) {
  return await DB.knex.select("gallery").from("user").where({ id_user });
}

async function getAllCustomers(search, orderBy = 'id_user', orderMethod = 'desc', perPage = 10, currentPage) {
  const query = DB.knex
    .select(
      "id_user",
      "email",
      "nama_lengkap",
      "nama_depan",
      "nama_belakang",
      "role",
      "no_hp",
      "asal_kota",
      "profesi",
      "tujuan",
      "minat",
      "nama_perusahaan",
      "kategori_perusahaan",
      "alamat_perusahaan",
      "foto_perusahaan",
      "foto_profil",
      "portofolio",
      "status"
    )
    .from("user").orderBy(orderBy, orderMethod);

  if (search) {
    query.where("email", "like", `${search}%`)
      .orWhere("nama_lengkap", "like", `${search}%`)
      .orWhere("nama_perusahaan", "like", `${search}%`);
  }
  return await query.paginate({
    perPage: perPage,
    currentPage: currentPage
  });
}


async function getAllPrincipal(search, orderBy = 'id_user', orderMethod = 'desc', perPage = 10, currentPage) {
  const query = DB.knex
    .select(
      "id_user",
      "email",
      "nama_lengkap",
      "nama_depan",
      "nama_belakang",
      "role",
      "no_hp",
      "asal_kota",
      "profesi",
      "tujuan",
      "minat",
      "nama_perusahaan",
      "kategori_perusahaan",
      "alamat_perusahaan",
      "foto_perusahaan",
      "foto_profil",
      "portofolio",
      "status",
      "youtube",
      "timestamp"
    )
    .from("user").where("role", "Prinsipal").orderBy(orderBy, orderMethod);

  if (search) {
    query.where("email", "like", `${search}%`)
      .orWhere("nama_lengkap", "like", `${search}%`)
      .orWhere("nama_perusahaan", "like", `${search}%`);
  }
  return await query.paginate({
    perPage: perPage,
    currentPage: currentPage
  });
}

async function getAllPrincipalBrand(search, orderBy = 'id_user', orderMethod = 'desc', perPage = 10, currentPage) {
  const query = DB.knex
    .select(
      "id_user",
      "nama_perusahaan",
      "foto_perusahaan",
      "status"
    )
    .from("user").where("role", "Prinsipal").where("foto_perusahaan", '!=' , "").orderBy(orderBy, orderMethod);

  if (search) {
    query.where("email", "like", `${search}%`)
      .orWhere("nama_lengkap", "like", `${search}%`)
      .orWhere("nama_perusahaan", "like", `${search}%`);
  }
  return await query.paginate({
    perPage: perPage,
    currentPage: currentPage
  });
}

async function getTotalAllCustomers(search) {
  const query = DB.knex('user');
  if (search) {
    query.where("email", "like", `${search}%`)
      .orWhere("nama_lengkap", "like", `${search}%`)
      .orWhere("nama_perusahaan", "like", `${search}%`);
  }
  return await query.count("id_user", { as: 'total' });
}

async function getTotalAllPrincipal(search) {
  const query = DB.knex('user').where("role", "Prinsipal");
  if (search) {
    query.where("email", "like", `${search}%`)
      .orWhere("nama_lengkap", "like", `${search}%`)
      .orWhere("nama_perusahaan", "like", `${search}%`);
  }
  return await query.count("id_user", { as: 'total' });
}

async function getTotalAllPrincipalBrand(search) {
  const query = DB.knex('user').where("role", "Prinsipal").where("foto_perusahaan", '!=' , "");
  if (search) {
    query.where("email", "like", `${search}%`)
      .orWhere("nama_lengkap", "like", `${search}%`)
      .orWhere("nama_perusahaan", "like", `${search}%`);
  }
  return await query.count("id_user", { as: 'total' });
}

async function getAllPartners() {
  return await DB.knex
    .select(
      "id_user",
      "asal_kota",
      "nama_perusahaan",
      "kategori_perusahaan",
      "alamat_perusahaan",
      "foto_perusahaan"
    )
    .from("user")
    .where("role", "Prinsipal")
    .where("foto_perusahaan", '!=' , "");
}

async function getPartnerData(id_user) {
  return await DB.knex
    .select(
      "no_hp",
      "asal_kota",
      "nama_perusahaan",
      "kategori_perusahaan",
      "alamat_perusahaan",
      "foto_perusahaan",
      "gallery",
      "portofolio",
      "youtube"
    )
    .from("user")
    .where({ id_user });
}
async function getPartnerFoto() {
  return await DB.knex
    .select(

      "foto_perusahaan",

    )
    .from("user")
    .where("foto_perusahaan", "!=", "")
}

async function verifyUserEmail(email) {
  await DB.knex("user").where({ email }).update({ status: 1 }).then();
}

async function verifyUserId(id_user) {
  return await DB.knex("user").where({ id_user }).update({ status: 1, role: "Prinsipal" }).then();
}

async function updateFoto(id_user, image) {
  return await DB.knex("user").where({ id_user }).update({ foto_profil: image }).then();
}

async function updateFotoPerusahaan(id_user, image) {
  return await DB.knex("user").where({ id_user }).update({ foto_perusahaan: image }).then();
}

async function updateGalleries(id_user, gallery) {
  await DB.knex("user").where({ id_user }).update({ gallery }).then();
}
async function updateGalleries(id_user, gallery) {
  await DB.knex("user").where({ id_user }).update({ gallery }).then();
}
async function getUserWishlistByUser(id_user) {
  return await DB.knex
    .select("p.*")
    .from(DB.knex.raw("products p, wishlist w"))
    .where(
      DB.knex.raw(`p.id_produk = w.id_product and w.id_user = ${id_user}`)
    );
}

async function getUserWishlistByItem(id_user, id_product) {
  return await DB.knex("wishlist").where({ id_user, id_product });
}

async function addWishlist(id_user, id_product) {
  return await DB.knex("wishlist").insert({ id_user, id_product });
}

async function removeWishlist(id_user, id_product) {
  return await DB.knex("wishlist").where({ id_user, id_product }).del().then();
}
async function getAllPrinsipal() {
  return await DB.knex.count('id_user').from('user').where('role', '=', 'Prinsipal');
}
async function getAllUsers() {
  return await DB.knex.select(
    "id_user",
    "email",
    "nama_lengkap",
    "nama_depan",
    "nama_belakang",
    "role",
    "no_hp",
    "asal_kota",
    "profesi",
    "tujuan",
    "minat",
    "nama_perusahaan",
    "kategori_perusahaan",
    "alamat_perusahaan",
    "foto_perusahaan",
    "foto_profil",
    "portofolio",
    "status").from('user');
}

async function getAllUsersNoticeNotCreate() {
  return await DB.knex.select("id_user as userid", "pocketbase_id as id", "notice_create as count").from('user').where('notice_create', '=', 0);
}

async function getAllUserIds() {
  return await DB.knex.select("id_user as userid", "pocketbase_id").from('user');
}

async function editUser(id_user, data) {
  data.timestamp = plugins.formatDate(Date.now());
  let result = await DB.knex("user").where({ id_user }).update(data).then();
  console.log(result);
}

async function deleteUser(id_user) {
  return await DB.knex("user").where({ id_user }).del().then();
}
async function deleteMaterial(id) {
  let result = await DB.knex("materials").where({ id }).del().then();
  console.log(result);
}
async function getAllMaterials(project_id) {
  return await DB.knex('materials').where({ project_id }).limit(100)
}
async function getProjectDetail(id) {
  return await DB.knex.select('p.*', 'm.*')
    .from(DB.knex.raw('projects p, materials m'))
    .where(DB.knex.raw(`p.id = m.project_id`))
    .groupBy('project_id');
}

const batchUpdate = async (options, collection) => {
  const { table, column } = options;
  const trx = await DB.knex.transaction();
  try {
    await Promise.all(collection.map(tuple =>
      DB.knex(table)
        .where(column, tuple[column])
        .update(tuple)
        .transacting(trx)
    )
    );
    await trx.commit();
  } catch (error) {
    console.log(error)
    await trx.rollback();
  }
}


async function listTypeByUser(email) {

  // join to project table ON materials.project_id = project.id
  return await DB.knex("user")
    .select(
      "user.*",
      DB.knex.raw(`CONCAT('[',GROUP_CONCAT(JSON_OBJECT(
 
      'tipe', products.tipe

  )),']') as products`)
    )
    .leftJoin("products", "user.id_user", "=", "products.user_id")
    .groupBy("user.id_user")
    .limit(1);

}

async function checkNewsLetter(email) {

  return await DB.knex("news_letter").where({email}).first();

}


async function addNewsLetter(data) {

  return await DB.knex("news_letter").insert(data);

}

module.exports = {
  login,
  loginSementara,
  listTypeByUser,
  register,
  getPartnerFoto,
  deleteMaterial,
  updatedataDiri,
  adminLogin,
  adminRegister,
  getUserData,
  verifyUserEmail,
  getAllCustomers,
  getAllUsers,
  getAllMaterials,
  getUserGalleries,
  getAllPartners,
  getPartnerData,
  updateGalleries,
  updatePassword,
  checkPassword,
  getUserPassword,
  getUserWishlistByUser,
  addWishlist,
  removeWishlist,
  getUserWishlistByItem,
  editUser,
  getAllPrinsipal,
  deleteUser,
  verifyUserId,
  forgetPassword,
  updateFoto,
  batchUpdate,
  getAllUsersNoticeNotCreate,
  getAllUserIds,
  updateFotoPerusahaan,
  getTotalAllCustomers,
  checkNewsLetter,
  addNewsLetter,
  getAllPrincipal,
  getAllPrincipalBrand,
  getUserByEmail,
  getTotalAllPrincipal,
  getTotalAllPrincipalBrand
};
