require('dotenv').config();
const DB = require('./index')
const plugins = require('../plugins');
const { counters } = require('sharp');

async function getAllArtikels(tipe = 'ARTIKEL', search, orderBy = 'id_artikel', orderMethod = 'desc', perPage = 10, currentPage) {

    const query = DB.knex('artikel').where('tipe', tipe).orderBy(orderBy, orderMethod);
    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("body", "like", `%${search}%`)
            .orWhere("category", "like", `${search}%`);
    }
    return await query.paginate({
        perPage: perPage,
        currentPage: currentPage
    });
}

async function getLatestArticle() {
    return await DB.knex('artikel').where('tipe', 'ARTIKEL').where('status', '=', 1).orderBy('id_artikel', 'desc').limit(4);
}

async function getTotalArtikels(tipe = 'ARTIKEL', search) {
    const query = DB.knex('artikel').where('tipe', tipe);
    if (search) {
        query.where("title", "like", `${search}%`)
            .orWhere("body", "like", `%${search}%`)
            .orWhere("category", "like", `${search}%`);
    }
    return await query.count("id_artikel", {as: 'total'});
}

async function getAllPublishArtikels(tipe = 'ARTIKEL') {
    return await DB.knex('artikel').where('status', '=', 1).andWhere('tipe', tipe).orderBy("id_artikel", "desc")
}

async function getArtikelsByPublished(status = 1) {
    return await DB.knex('artikel').where('id_artikel', '!=', -1).andWhere('status', status)
}

async function getArtikelById(id_artikel) {
    return await DB.knex('artikel').where({ id_artikel })
}

async function addArtikel(data) {
    data.timestamp = plugins.formatDate(Date.now())
    let result = await DB.knex('artikel').insert(data)
    console.log(result)
}

async function editArtikel(id_artikel, data) {
    data.timestamp = plugins.formatDate(Date.now())
    let result = await DB.knex('artikel').where({ id_artikel }).update(data).then();
    console.log(result)
}

async function deleteArtikel(id_artikel) {
    let result = await DB.knex('artikel').where({ id_artikel }).del().then();
    console.log(result)
}

module.exports = {
    getAllArtikels,
    getArtikelsByPublished,
    addArtikel,
    editArtikel,
    deleteArtikel,
    getArtikelById,
    getAllPublishArtikels,
    getTotalArtikels,
    getLatestArticle,
}